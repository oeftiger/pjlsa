
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.accsoft.commons.value
import cern.accsoft.commons.value.json.common
import cern.accsoft.commons.value.json.function.polynomial
import cern.accsoft.commons.value.json.function.polynomial.bounded.sequence
import jpype
import typing



class BoundedPolynomialData:
    """
    public class BoundedPolynomialData extends java.lang.Object
    """
    def __init__(self, doubleArray: typing.Union[typing.List[float], jpype.JArray], double2: float, double3: float): ...
    def getCoefficients(self) -> typing.MutableSequence[float]: ...
    def getLowerBound(self) -> float: ...
    def getUpperBound(self) -> float: ...

_BoundedPolynomialDto__R = typing.TypeVar('_BoundedPolynomialDto__R')  # <R>
class BoundedPolynomialDto(cern.accsoft.commons.value.json.function.polynomial.PolynomialDto[cern.accsoft.commons.value.BoundedPolynomial, _BoundedPolynomialDto__R], typing.Generic[_BoundedPolynomialDto__R]):
    """
    public abstract class BoundedPolynomialDto<R> extends :class:`~cern.accsoft.commons.value.json.function.polynomial.PolynomialDto`<cern.accsoft.commons.value.BoundedPolynomial, R>
    """
    ...

class ToBoundedPolynomialDto(cern.accsoft.commons.value.json.common.TypedToDtoConverter[cern.accsoft.commons.value.BoundedPolynomial, 'BoundedPolynomialImplDto']):
    """
    public class ToBoundedPolynomialDto extends java.lang.Object implements :class:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter`<cern.accsoft.commons.value.BoundedPolynomial, :class:`~cern.accsoft.commons.value.json.function.polynomial.bounded.BoundedPolynomialImplDto`>
    """
    def __init__(self): ...
    def convert(self, boundedPolynomial: cern.accsoft.commons.value.BoundedPolynomial) -> 'BoundedPolynomialImplDto':
        """
        
            Specified by:
                :meth:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter.convert`Â in
                interfaceÂ :class:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter`
        
        
        """
        ...

class BoundedPolynomialImplDto(BoundedPolynomialDto[BoundedPolynomialData]):
    """
    public class BoundedPolynomialImplDto extends :class:`~cern.accsoft.commons.value.json.function.polynomial.bounded.BoundedPolynomialDto`<:class:`~cern.accsoft.commons.value.json.function.polynomial.bounded.BoundedPolynomialData`>
    """
    ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.value.json.function.polynomial.bounded")``.

    BoundedPolynomialData: typing.Type[BoundedPolynomialData]
    BoundedPolynomialDto: typing.Type[BoundedPolynomialDto]
    BoundedPolynomialImplDto: typing.Type[BoundedPolynomialImplDto]
    ToBoundedPolynomialDto: typing.Type[ToBoundedPolynomialDto]
    sequence: cern.accsoft.commons.value.json.function.polynomial.bounded.sequence.__module_protocol__
