
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.accsoft.commons.value
import cern.accsoft.commons.value.json.common
import cern.accsoft.commons.value.json.function
import cern.accsoft.commons.value.json.function.discrete.array
import cern.accsoft.commons.value.json.function.discrete.list
import cern.accsoft.commons.value.json.scalar
import jpype
import typing



_DiscreteFunctionDto__D = typing.TypeVar('_DiscreteFunctionDto__D', bound=cern.accsoft.commons.value.DiscreteFunction)  # <D>
_DiscreteFunctionDto__R = typing.TypeVar('_DiscreteFunctionDto__R')  # <R>
class DiscreteFunctionDto(cern.accsoft.commons.value.json.scalar.ScalarDto[_DiscreteFunctionDto__D, _DiscreteFunctionDto__R], cern.accsoft.commons.value.json.function.FunctionDto[_DiscreteFunctionDto__D], typing.Generic[_DiscreteFunctionDto__D, _DiscreteFunctionDto__R]):
    """
    public abstract class DiscreteFunctionDto<D extends cern.accsoft.commons.value.DiscreteFunction, R> extends :class:`~cern.accsoft.commons.value.json.scalar.ScalarDto`<D, R> implements :class:`~cern.accsoft.commons.value.json.function.FunctionDto`<D>
    """
    ...

class FunctionArrayData:
    """
    public class FunctionArrayData extends java.lang.Object
    """
    def __init__(self, doubleArray: typing.Union[typing.List[float], jpype.JArray], doubleArray2: typing.Union[typing.List[float], jpype.JArray]): ...
    def getX(self) -> typing.MutableSequence[float]: ...
    def getY(self) -> typing.MutableSequence[float]: ...

class ToDiscreteFunctionArrayDto(cern.accsoft.commons.value.json.common.TypedToDtoConverter[cern.accsoft.commons.value.ImmutableDiscreteFunction, 'DiscreteFunctionArrayDto']):
    """
    public class ToDiscreteFunctionArrayDto extends java.lang.Object implements :class:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter`<cern.accsoft.commons.value.ImmutableDiscreteFunction, :class:`~cern.accsoft.commons.value.json.function.discrete.DiscreteFunctionArrayDto`>
    """
    def __init__(self): ...
    def convert(self, immutableDiscreteFunction: cern.accsoft.commons.value.ImmutableDiscreteFunction) -> 'DiscreteFunctionArrayDto':
        """
        
            Specified by:
                :meth:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter.convert`Â in
                interfaceÂ :class:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter`
        
        
        """
        ...

class ToDiscreteFunctionPointDto(cern.accsoft.commons.value.json.common.TypedToDtoConverter[cern.accsoft.commons.value.ImmutableDiscreteFunction, 'DiscreteFunctionPointDto']):
    """
    public class ToDiscreteFunctionPointDto extends java.lang.Object implements :class:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter`<cern.accsoft.commons.value.ImmutableDiscreteFunction, :class:`~cern.accsoft.commons.value.json.function.discrete.DiscreteFunctionPointDto`>
    """
    def __init__(self): ...
    def convert(self, immutableDiscreteFunction: cern.accsoft.commons.value.ImmutableDiscreteFunction) -> 'DiscreteFunctionPointDto':
        """
        
            Specified by:
                :meth:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter.convert`Â in
                interfaceÂ :class:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter`
        
        
        """
        ...

class DiscreteFunctionArrayDto(DiscreteFunctionDto[cern.accsoft.commons.value.DiscreteFunction, FunctionArrayData]):
    """
    public class DiscreteFunctionArrayDto extends :class:`~cern.accsoft.commons.value.json.function.discrete.DiscreteFunctionDto`<cern.accsoft.commons.value.DiscreteFunction, :class:`~cern.accsoft.commons.value.json.function.discrete.FunctionArrayData`>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, functionArrayData: FunctionArrayData): ...

class DiscreteFunctionPointDto(DiscreteFunctionDto[cern.accsoft.commons.value.DiscreteFunction, typing.MutableSequence[cern.accsoft.commons.value.json.scalar.PointData]]):
    """
    public class DiscreteFunctionPointDto extends :class:`~cern.accsoft.commons.value.json.function.discrete.DiscreteFunctionDto`<cern.accsoft.commons.value.DiscreteFunction, :class:`~cern.accsoft.commons.value.json.scalar.PointData`[]>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, pointDataArray: typing.Union[typing.List[cern.accsoft.commons.value.json.scalar.PointData], jpype.JArray]): ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.value.json.function.discrete")``.

    DiscreteFunctionArrayDto: typing.Type[DiscreteFunctionArrayDto]
    DiscreteFunctionDto: typing.Type[DiscreteFunctionDto]
    DiscreteFunctionPointDto: typing.Type[DiscreteFunctionPointDto]
    FunctionArrayData: typing.Type[FunctionArrayData]
    ToDiscreteFunctionArrayDto: typing.Type[ToDiscreteFunctionArrayDto]
    ToDiscreteFunctionPointDto: typing.Type[ToDiscreteFunctionPointDto]
    array: cern.accsoft.commons.value.json.function.discrete.array.__module_protocol__
    list: cern.accsoft.commons.value.json.function.discrete.list.__module_protocol__
