
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.accsoft.commons.value
import cern.accsoft.commons.value.json.common
import jpype
import typing



_DiscreteFunctionsArrayDto__R = typing.TypeVar('_DiscreteFunctionsArrayDto__R')  # <R>
class DiscreteFunctionsArrayDto(cern.accsoft.commons.value.json.common.ValueDto[cern.accsoft.commons.value.DiscreteFunctionsArray, _DiscreteFunctionsArrayDto__R], typing.Generic[_DiscreteFunctionsArrayDto__R]):
    """
    public abstract class DiscreteFunctionsArrayDto<R> extends :class:`~cern.accsoft.commons.value.json.common.ValueDto`<cern.accsoft.commons.value.DiscreteFunctionsArray, R>
    """
    ...

class ToDiscreteFunctionsArrayDto(cern.accsoft.commons.value.json.common.TypedToDtoConverter[cern.accsoft.commons.value.ImmutableDiscreteFunctionsArray, 'DiscreteFunctionsArrayImplDto']):
    """
    public class ToDiscreteFunctionsArrayDto extends java.lang.Object implements :class:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter`<cern.accsoft.commons.value.ImmutableDiscreteFunctionsArray, :class:`~cern.accsoft.commons.value.json.function.discrete.array.DiscreteFunctionsArrayImplDto`>
    """
    def __init__(self): ...
    def convert(self, immutableDiscreteFunctionsArray: cern.accsoft.commons.value.ImmutableDiscreteFunctionsArray) -> 'DiscreteFunctionsArrayImplDto':
        """
        
            Specified by:
                :meth:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter.convert`Â in
                interfaceÂ :class:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter`
        
        
        """
        ...

class DiscreteFunctionsArrayImplDto(DiscreteFunctionsArrayDto[typing.MutableSequence[cern.accsoft.commons.value.ImmutableDiscreteFunction]]):
    """
    public class DiscreteFunctionsArrayImplDto extends :class:`~cern.accsoft.commons.value.json.function.discrete.array.DiscreteFunctionsArrayDto`<cern.accsoft.commons.value.ImmutableDiscreteFunction[]>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, immutableDiscreteFunctionArray: typing.Union[typing.List[cern.accsoft.commons.value.ImmutableDiscreteFunction], jpype.JArray]): ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.value.json.function.discrete.array")``.

    DiscreteFunctionsArrayDto: typing.Type[DiscreteFunctionsArrayDto]
    DiscreteFunctionsArrayImplDto: typing.Type[DiscreteFunctionsArrayImplDto]
    ToDiscreteFunctionsArrayDto: typing.Type[ToDiscreteFunctionsArrayDto]
