
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.accsoft.commons.value
import typing



_TypedDto__D = typing.TypeVar('_TypedDto__D', bound=cern.accsoft.commons.value.Typed)  # <D>
class TypedDto(typing.Generic[_TypedDto__D]):
    """
    public interface TypedDto<D extends cern.accsoft.commons.value.Typed>
    """
    def toDomain(self) -> _TypedDto__D: ...

_TypedToDtoConverter__T = typing.TypeVar('_TypedToDtoConverter__T', bound=cern.accsoft.commons.value.Typed)  # <T>
_TypedToDtoConverter__D = typing.TypeVar('_TypedToDtoConverter__D', bound=TypedDto)  # <D>
class TypedToDtoConverter(typing.Generic[_TypedToDtoConverter__T, _TypedToDtoConverter__D]):
    """
    public interface TypedToDtoConverter<T extends cern.accsoft.commons.value.Typed, D extends :class:`~cern.accsoft.commons.value.json.common.TypedDto`<? extends T>>
    """
    def convert(self, t: _TypedToDtoConverter__T) -> _TypedToDtoConverter__D: ...

class ToTypedDto(TypedToDtoConverter[cern.accsoft.commons.value.Typed, TypedDto[cern.accsoft.commons.value.Typed]]):
    """
    public class ToTypedDto extends java.lang.Object implements :class:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter`<cern.accsoft.commons.value.Typed, :class:`~cern.accsoft.commons.value.json.common.TypedDto`<? extends cern.accsoft.commons.value.Typed>>
    """
    def __init__(self): ...
    def convert(self, typed: cern.accsoft.commons.value.Typed) -> TypedDto[cern.accsoft.commons.value.Typed]:
        """
        
            Specified by:
                :meth:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter.convert`Â in
                interfaceÂ :class:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter`
        
        
        """
        ...

_ValueDto__D = typing.TypeVar('_ValueDto__D', bound=cern.accsoft.commons.value.Value)  # <D>
_ValueDto__R = typing.TypeVar('_ValueDto__R')  # <R>
class ValueDto(TypedDto[_ValueDto__D], typing.Generic[_ValueDto__D, _ValueDto__R]):
    """
    public abstract class ValueDto<D extends cern.accsoft.commons.value.Value, R> extends java.lang.Object implements :class:`~cern.accsoft.commons.value.json.common.TypedDto`<D>
    """
    def getRawValue(self) -> _ValueDto__R: ...
    def getType(self) -> cern.accsoft.commons.value.Type: ...
    def getValueDescriptor(self) -> cern.accsoft.commons.value.ValueDescriptor: ...
    def toDomain(self) -> _ValueDto__D:
        """
        
            Specified by:
                :meth:`~cern.accsoft.commons.value.json.common.TypedDto.toDomain`Â in
                interfaceÂ :class:`~cern.accsoft.commons.value.json.common.TypedDto`
        
        
        """
        ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.value.json.common")``.

    ToTypedDto: typing.Type[ToTypedDto]
    TypedDto: typing.Type[TypedDto]
    TypedToDtoConverter: typing.Type[TypedToDtoConverter]
    ValueDto: typing.Type[ValueDto]
