
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.accsoft.ccs.ccda.client.accelerator
import cern.accsoft.ccs.ccda.client.accountegroup
import cern.accsoft.ccs.ccda.client.ccm
import cern.accsoft.ccs.ccda.client.ccm.enums
import cern.accsoft.ccs.ccda.client.core
import cern.accsoft.ccs.ccda.client.device
import cern.accsoft.ccs.ccda.client.fesa
import cern.accsoft.ccs.ccda.client.hardware
import cern.accsoft.ccs.ccda.client.model.accelerator
import cern.accsoft.ccs.ccda.client.model.device
import cern.accsoft.commons.domain
import cern.accsoft.commons.util
import cern.japc.value
import java.io
import java.lang
import java.util
import jpype
import typing



class AcceleratorInfo(cern.accsoft.commons.util.Named):
    """
    public final class AcceleratorInfo extends java.lang.Object implements cern.accsoft.commons.util.Named
    
        This class provides utility methods to handle AcceleratorInfo device information such as:
    
          - The accelerator.INFO device name.
          - The accelerator.INFO associated :code:`CernAccelerator`.
          - The accelerator.INFO associated lsaAccelerator name.
          - The accelerator.INFO associated ccdbAccelerator name.
          - The accelerator.INFO associated tgmMachine name.
          - The accelerator.INFO associated non Multiplexed Context name.
          - The accelerator.INFO associated lsaAccelerator name.
    
        More details can be found here: https://wikis.cern.ch/display/InCA/LsaContextMappingMonitor+marked+as+deprecated
    """
    def __init__(self, device: cern.accsoft.ccs.ccda.client.model.device.Device): ...
    @staticmethod
    def ccdbTolsaAccelerator(string: str) -> str:
        """
            Return the CCDB accelerator name associated to the given LSA one.
        
            Parameters:
                acceleratorName (java.lang.String): The LSA accelerator name to check.
        
            Returns:
                the CCDB accelerator name associated to the given LSA one.
        
        
        """
        ...
    @staticmethod
    def getAcceleratorFromSystemProperty() -> cern.accsoft.commons.domain.CernAccelerator:
        """
        
            Returns:
                The :code:`CernAccelerator` from the :meth:`~cern.accsoft.commons.ccs.AcceleratorInfo.ACCELERATOR_SYSTEM_PROPERTY`
                system property or null if no matching :code:`CernAccelerator` can be found.
        
        
        """
        ...
    @staticmethod
    def getAcceleratorInfoForCernAccelerator(cernAccelerator: cern.accsoft.commons.domain.CernAccelerator) -> 'AcceleratorInfo':
        """
        
            Parameters:
                cernAccelerator (cern.accsoft.commons.domain.CernAccelerator):             The :code:`CernAccelerator` used to retrieve its associated :class:`~cern.accsoft.commons.ccs.AcceleratorInfo` instance.
        
            Returns:
                The :class:`~cern.accsoft.commons.ccs.AcceleratorInfo` instance associated to the given :code:`CernAccelerator`.
        
        
        """
        ...
    def getCcdbAccelerator(self) -> str:
        """
        
            Returns:
                The xxx.INFO CCDB accelerator name.
        
        
                Can be null in case of ASYNC.
        
        
        """
        ...
    @staticmethod
    def getCcdbAcceleratorForOpConfig(string: str) -> str:
        """
            Return the CCDB accelerator name associated to the given operational configuration.
        
        
            Notice that it can be null.
        
            Parameters:
                opConfigName (java.lang.String): The operational configuration to check.
        
            Returns:
                the CCDB accelerator name associated to the given operational configuration.
        
        
        """
        ...
    def getCernAccelerator(self) -> cern.accsoft.commons.domain.CernAccelerator:
        """
        
            Returns:
                The CernAccelerator stored in this object.
        
        
                Can be null in case of ASYNC.
        
        
        """
        ...
    @staticmethod
    def getCernAcceleratorForName(string: str) -> cern.accsoft.commons.domain.CernAccelerator:
        """
        
            Parameters:
                acceleratorName (java.lang.String): The LSA accelerator name or CCDB accelerator code.
        
            Returns:
                The :code:`CernAccelerator` Object associated with the given accelerator name.
        
        
        """
        ...
    @staticmethod
    def getCernAcceleratorForOpConfig(string: str) -> cern.accsoft.commons.domain.CernAccelerator:
        """
            Returns The :code:`CernAccelerator` associated with the given opConfig name.
        
        
        
            Parameters:
                opConfigName (java.lang.String): The opConfig name.
        
            Returns:
                The :code:`CernAccelerator` associated with the given opConfig name.
        
        
        """
        ...
    @staticmethod
    def getCernAcceleratorForTgm(string: str) -> cern.accsoft.commons.domain.CernAccelerator:
        """
            Return the :code:`CernAccelerator` name associated to the given tgmMachineName.
        
        
            Notice that it can be null.
        
            **IMPORTANT:** This method is existing only for backward compatibility but it may return wrong value if used in a
            NON-INCA environment as since LS2 several Accelerators have the same TGM machine.
        
        
            Therefore, I am strongly advising you to use the following methods instead:
        
              - :meth:`~cern.accsoft.commons.ccs.AcceleratorInfo.getCernAcceleratorForOpConfig`
              - :meth:`~cern.accsoft.commons.ccs.AcceleratorInfo.getCernAcceleratorForName`
        
        
            Parameters:
                tgmString (java.lang.String): Can be TGM machine name or a PLS condition/equation
        
            Returns:
                A :code:`CernAccelerator` according to the possible following information sources:
        
                  - The given tgmString can be TGM machine name.
                  - The given tgmString can be a PLS condition/equation.
        
        
            Also see:
                :meth:`~cern.accsoft.commons.ccs.AcceleratorInfo.getCernAcceleratorForOpConfig`,
                :meth:`~cern.accsoft.commons.ccs.AcceleratorInfo.getCernAcceleratorForName`
        
        
        """
        ...
    @staticmethod
    def getDefaultCernAccelerator() -> cern.accsoft.commons.domain.CernAccelerator:
        """
        
            Returns:
                The :code:`CernAccelerator` "guessed" according to the possible following information sources:
        
                  - The "accelerator" system property contents.
                  - The accelerator associated to the current "opConfig" (from system property, environment variable, SHREG,etc).
        
        
        
        """
        ...
    @staticmethod
    def getInfoDeviceForCernAccelerator(cernAccelerator: cern.accsoft.commons.domain.CernAccelerator) -> str:
        """
        
            Parameters:
                cernAccelerator (cern.accsoft.commons.domain.CernAccelerator): The :code:`CernAccelerator` used to retrieve its associated accelerator.INFO device name.
        
            Returns:
                The accelerator.INFO device name associated with the given accelerator, can be null if given accelerator is null
        
        
        """
        ...
    @staticmethod
    def getInfoDeviceForLsaAcceleratorName(string: str) -> str:
        """
            Return the xxx.INFO device name associated to the given LSA acceleratorName.
        
        
            Notice that it can be null.
        
            Parameters:
                lsaAcceleratorName (java.lang.String): The LSA accelerator name to check.
        
            Returns:
                the xxx.INFO device name associated to the given LSA acceleratorName.
        
        
        """
        ...
    def getLsaAccelerator(self) -> str:
        """
        
            Returns:
                The xxx.INFO LSA accelerator name.
        
        
                Can be null in case of ASYNC.
        
        
        """
        ...
    @staticmethod
    def getLsaAcceleratorForCernAccelerator(cernAccelerator: cern.accsoft.commons.domain.CernAccelerator) -> str:
        """
            Return the LSA accelerator name associated to the given operational configuration.
        
        
            Notice that it can be null.
        
            Parameters:
                cernAccelerator (cern.accsoft.commons.domain.CernAccelerator): The :code:`CernAccelerator` used to retrieve its associated Lsa Accelerator name.
        
            Returns:
                the LSA accelerator name associated to the given :code:`CernAccelerator`.
        
            Also see:
                :meth:`~cern.accsoft.commons.ccs.AcceleratorInfo.getLsaAcceleratorForOpConfig`
        
        
        """
        ...
    @staticmethod
    def getLsaAcceleratorForOpConfig(string: str) -> str:
        """
            Return the LSA accelerator name associated to the given operational configuration.
        
        
            Notice that it can be null.
        
            Parameters:
                opConfigName (java.lang.String): The operational configuration to check.
        
            Returns:
                the LSA accelerator name associated to the given operational configuration.
        
            Also see:
                :meth:`~cern.accsoft.commons.ccs.AcceleratorInfo.getLsaAcceleratorForCernAccelerator`
        
        
        """
        ...
    def getName(self) -> str:
        """
        
            Specified by:
                :code:`getName` in interface :code:`cern.accsoft.commons.util.Named`
        
        
        """
        ...
    @staticmethod
    def getNonMultiplexContextNameForAccelerator(string: str) -> str:
        """
        
            Parameters:
                acceleratorName (java.lang.String): The (lsa / ccdb) accelerator name used to retrieve its associated :class:`~cern.accsoft.commons.ccs.AcceleratorInfo`
                    instance Non Multiplexed Context name.
        
            Returns:
                The :class:`~cern.accsoft.commons.ccs.AcceleratorInfo` instance Non Multiplexed Context name associated to the given
                (lsa / ccdb) accelerator name.
        
        
        """
        ...
    def getNonMultiplexedContext(self) -> str:
        """
        
            Returns:
                The xxx.INFO non-multiplex Context name.
        
        
                Can be null in case of ASYNC.
        
        
        """
        ...
    @staticmethod
    def getOpConfig() -> str:
        """
        
            Returns:
                The current OpConfig if defined.
        
        
        """
        ...
    def getOpConfigs(self) -> java.util.List[str]:
        """
        
            Returns:
                And unmodifiable list of OpConfig names.
        
        
        """
        ...
    def getTgmMachine(self) -> str:
        """
        
            Returns:
                The xxx.INFO TGM machine name.
        
        
                Can never be null.
        
        
        """
        ...
    @staticmethod
    def getTgmMachineNameForOpConfig(string: str) -> str:
        """
            Return the TGM machine name associated to the given operational configuration.
        
        
            Notice that it can be null.
        
            Parameters:
                opConfigName (java.lang.String): The operational configuration to check.
        
            Returns:
                the TGM machine name associated to the given operational configuration.
        
        
        """
        ...
    @staticmethod
    def isCcdbAcceleratorName(string: str) -> bool:
        """
            Return true if the given accelerator name is a CCDB one, false otherwise.
        
            Parameters:
                acceleratorName (java.lang.String): The accelerator name to check.
        
            Returns:
                true if the given accelerator name is a CCDB one, false otherwise.
        
        
        """
        ...
    @staticmethod
    def isLsaAcceleratorName(string: str) -> bool:
        """
            Return true if the given accelerator name is a LSA one, false otherwise.
        
            Parameters:
                acceleratorName (java.lang.String): The accelerator name to check.
        
            Returns:
                true if the given accelerator name is a LSA one, false otherwise.
        
        
        """
        ...
    @staticmethod
    def isPseudoNonMultiplexedAccelerator(cernAccelerator: cern.accsoft.commons.domain.CernAccelerator) -> bool:
        """
        
            Parameters:
                cernAccelerator (cern.accsoft.commons.domain.CernAccelerator): The :code:`CernAccelerator` to determine if we are dealing with a "special" accelerator which is using a
                    _NON_MULTIPLEXED_CONTEXT_XXX as Context no matter the given PLS Condition or not.
        
        
        Like CTF, ISOLDE, AD but not the LHC as it has not a "cycling" TimingDomain
        
            Returns:
                true if the given :code:`CernAccelerator` is a "special" accelerator, false otherwise.
        
        
        """
        ...
    @staticmethod
    def isTgmMachineNameForOpConfigExisting(string: str) -> bool:
        """
            Return true if the given opConfigName has a TGM machine associated, false otherwise.
        
            Parameters:
                opConfigName (java.lang.String): the opConfig name used to retreive its TGM machine.
        
            Returns:
                true if the given opConfigName has a TGM machine associated, false otherwise.
        
        
        """
        ...
    @staticmethod
    def lsaToCcdbAccelerator(string: str) -> str:
        """
            Return the CCDB accelerator name associated to the given LSA one.
        
            Parameters:
                acceleratorName (java.lang.String): The LSA accelerator name to check.
        
            Returns:
                the CCDB accelerator name associated to the given LSA one.
        
        
        """
        ...
    def toString(self) -> str:
        """
        
            Overrides:
                :code:`toString` in class :code:`java.lang.Object`
        
        
        """
        ...
    @staticmethod
    def values() -> java.util.Collection['AcceleratorInfo']: ...

class CcdaAccess:
    SYSPROP_CCDA_ENVIRONMENT: typing.ClassVar[str] = ...
    @staticmethod
    def getCcdaClient() -> cern.accsoft.ccs.ccda.client.core.CcdaClient: ...
    @staticmethod
    def getDefaultCcdaEnvironment() -> cern.accsoft.ccs.ccda.client.core.Environment: ...
    @staticmethod
    def getEnvironmentFromSystemProperty() -> cern.accsoft.ccs.ccda.client.core.Environment: ...

class CcdaUtils:
    """
    public class CcdaUtils extends java.lang.Object
    """
    DEFAULT_VALUE_FIELD: typing.ClassVar[str] = ...
    """
    public static final java.lang.String DEFAULT_VALUE_FIELD
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    UNDEFINED: typing.ClassVar[str] = ...
    """
    public static final java.lang.String UNDEFINED
    
        Default value when there is no CCS value defined.
    
        Also see:
            :meth:`~constant`
    
    
    """
    DEFAULT_DIMENSION: typing.ClassVar[int] = ...
    """
    public static final int DEFAULT_DIMENSION
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    @staticmethod
    def calculateFormatPattern(simpleDescriptor: cern.japc.value.SimpleDescriptor) -> str: ...
    @staticmethod
    def createBooleanType(string: str, propertyFieldBooleanMeaning: cern.accsoft.ccs.ccda.client.model.device.PropertyFieldBooleanMeaning) -> cern.japc.value.BooleanType: ...
    @staticmethod
    def createEnumOrBooleanTypeName(device: cern.accsoft.ccs.ccda.client.model.device.Device, deviceClassProperty: cern.accsoft.ccs.ccda.client.model.device.DeviceClassProperty, propertyField: cern.accsoft.ccs.ccda.client.model.device.PropertyField) -> str: ...
    @staticmethod
    def createEnumType(string: str, valueType: cern.japc.value.ValueType, propertyField: cern.accsoft.ccs.ccda.client.model.device.PropertyField) -> cern.japc.value.EnumType: ...
    @staticmethod
    def getAccelerator(string: str) -> cern.accsoft.ccs.ccda.client.model.accelerator.Accelerator:
        """
        
            Parameters:
                acceleratorName (java.lang.String): The name use to retrieve its associated :code:`Accelerator` from the CCDB.
        
            Returns:
                The :code:`Accelerator` object associated to the given name.
        
        
        """
        ...
    @staticmethod
    def getAcceleratorNames() -> java.util.Set[str]:
        """
        
            Returns:
                All existing CCDB Accelerators names.
        
        
        """
        ...
    @staticmethod
    def getAcceleratorService() -> cern.accsoft.ccs.ccda.client.accelerator.AcceleratorService:
        """
        
            Returns:
                a reference on the :code:`AcceleratorService` used in this JVM.
        
        
        """
        ...
    @staticmethod
    def getCcdaClient() -> cern.accsoft.ccs.ccda.client.core.CcdaClient:
        """
        
            Returns:
                a reference on the :code:`CcdaClient` used in this JVM.
        
        
        """
        ...
    @staticmethod
    def getColumnCount(propertyField: cern.accsoft.ccs.ccda.client.model.device.PropertyField) -> int:
        """
        
            Parameters:
                propertyField (cern.accsoft.ccs.ccda.client.model.device.PropertyField): :code:`PropertyField` to get the column count from.
        
            Returns:
                The the column count if defined or 1 if not defined.
        
        
        """
        ...
    @staticmethod
    def getComputer(string: str) -> cern.accsoft.ccs.ccda.client.hardware.Computer:
        """
        
            Parameters:
                computerName (java.lang.String): The CCDA :code:`Computer` name.
        
            Returns:
                CCDA :code:`Computer` for given name or null if the computer name cannot be found
        
        
        """
        ...
    @staticmethod
    def getComputerService() -> cern.accsoft.ccs.ccda.client.hardware.ComputerService:
        """
        
            Returns:
                a reference on the :code:`ComputerService` used in this JVM.
        
        
        """
        ...
    @staticmethod
    def getDevice(string: str) -> cern.accsoft.ccs.ccda.client.model.device.Device:
        """
            Returns the device associated with given name or alias.
        
            Parameters:
                deviceNameOrAlias (java.lang.String): The device name or alias used to retrieve the associated Device Object.
        
        
        """
        ...
    @staticmethod
    def getDeviceAcceleratorName(string: str) -> str:
        """
        
            Parameters:
                deviceName (java.lang.String): The device name used to retrieve its associated accelerator from.
        
            Returns:
                The accelerator associated with the given device name. @ in case of problem accessing the CCDB.
        
        
        """
        ...
    @staticmethod
    def getDeviceClassForDevice(string: str) -> cern.accsoft.ccs.ccda.client.model.device.DeviceClass:
        """
        
            Parameters:
                deviceName (java.lang.String): The device name used to retrieve its associated :code:`DeviceClass` from
        
            Returns:
                the :code:`DeviceClass` associated with the given device name, could be empty if the device does not exists.
        
        
        """
        ...
    @staticmethod
    def getDeviceClassNamesByAccelerator(string: str) -> java.util.Set[str]:
        """
        
            Parameters:
                acceleratorName (java.lang.String): if null the method will be applied on ALL accelerator Names.
        
            Returns:
                All existing device class names for the given accelerator.
        
        
        """
        ...
    @staticmethod
    def getDeviceClassNamesByAcceleratorAndFrontEnds(string: str, stringArray: typing.Union[typing.List[str], jpype.JArray]) -> java.util.Set[str]:
        """
        
            Parameters:
                ccdbAccelerator (java.lang.String): if null the method will be applied on accelerators.
                fecArray (java.lang.String[]): if null or empty the method will be applied on ALL front ends.
        
            Returns:
                All existing device class names for the given CCDB accelerator.
        
        
        """
        ...
    @staticmethod
    def getDeviceClassNamesByFrontEnd(string: str) -> java.util.Set[str]:
        """
        
            Parameters:
                frontEndName (java.lang.String): if null the method will be applied on ALL front ends.
        
            Returns:
                All existing device class names for the given front end.
        
        
        """
        ...
    @staticmethod
    def getDeviceClassService() -> cern.accsoft.ccs.ccda.client.device.DeviceClassService:
        """
        
            Returns:
                a reference on the :code:`DeviceClassService` used in this JVM.
        
        
        """
        ...
    @staticmethod
    def getDeviceNamesByClassNameFrontEndsAndAccelerators(string: str, stringArray: typing.Union[typing.List[str], jpype.JArray], stringArray2: typing.Union[typing.List[str], jpype.JArray]) -> java.util.Set[str]:
        """
            Return a String array containing all the device names matching the method argument filters.
        
            Parameters:
                deviceClassName (java.lang.String): if null the method will be applied on ALL classes.
                fecArray (java.lang.String[]): if null or empty the method will be applied on ALL front ends.
                ccdbAccelerators (java.lang.String[]): if null or empty the method will be applied on ALL CCDB accelerators. (CCDB Accelerator == LSA Accelerator code)
        
            Returns:
                All existing device names matching the method argument filters.
        
        
        """
        ...
    @staticmethod
    def getDeviceNamesByClassNamesAndAccelerators(stringArray: typing.Union[typing.List[str], jpype.JArray], stringArray2: typing.Union[typing.List[str], jpype.JArray]) -> java.util.Set[str]:
        """
        
            Parameters:
                ccdbAccelerators (java.lang.String[]): if null or empty the method will be applied on ALL CCDB accelerators. (CCDB Accelerator == LSA Accelerator code)
                deviceClassNames (java.lang.String[]): if null or empty the method will be applied on ALL classes.
        
            Returns:
                All existing device names matching the method argument filters.
        
        
        """
        ...
    @staticmethod
    def getDeviceService() -> cern.accsoft.ccs.ccda.client.device.DeviceService:
        """
        
            Returns:
                a reference on the :code:`DeviceService` used in this JVM.
        
        
        """
        ...
    @staticmethod
    def getDeviceVersion(string: str) -> str:
        """
            Returns the device version as it is stored in the CCDB.
        
            Parameters:
                deviceName (java.lang.String): The device name used to retrieve the associated version String.
        
        
        """
        ...
    @staticmethod
    def getFesaClassProperty(string: str, string2: str) -> cern.accsoft.ccs.ccda.client.fesa.FesaClassProperty:
        """
        
            Parameters:
                deviceName (java.lang.String): The device name used to found its associated :code:`FesaClassProperty`.
                propertyName (java.lang.String): The property name used to found its associated :code:`FesaClassProperty`.
        
            Returns:
                The :code:`FesaClassProperty` associated with the given device and property names or null if the device is not a a FESA
                one or the device or the property does not exists.
        
        
        """
        ...
    @staticmethod
    def getFesaService() -> cern.accsoft.ccs.ccda.client.fesa.FesaService:
        """
        
            Returns:
                a reference on the :code:`FesaService` used in this JVM.
        
        
        """
        ...
    @staticmethod
    def getFieldOfSimpleProperty(deviceClassProperty: cern.accsoft.ccs.ccda.client.model.device.DeviceClassProperty) -> cern.accsoft.ccs.ccda.client.model.device.PropertyField: ...
    @staticmethod
    def getFieldsForDeviceProperty(string: str, string2: str) -> java.util.Set[str]:
        """
        
            Parameters:
                deviceName (java.lang.String): The device name used to retrieve its associated property fields from.
                propertyName (java.lang.String): The device name used to retrieve its associated property fields from.
        
            Returns:
                A Set of property fields associated with the given device name, could be empty if the device or the property name does
                not exists.
        
        
        """
        ...
    @staticmethod
    def getFilterFieldsForDeviceProperty(string: str, string2: str) -> java.util.Set[str]:
        """
        
            Parameters:
                deviceName (java.lang.String): The device name used to retrieve its associated property fields from.
                propertyName (java.lang.String): The device name used to retrieve its associated property fields from.
        
            Returns:
                A Set of property fields associated with the given device name, could be empty if the device or the property name does
                not exists.
        
        
        """
        ...
    @staticmethod
    def getFilterFieldsForFesaDeviceProperty(string: str, string2: str) -> java.util.Set[str]: ...
    @staticmethod
    def getFrontEndNamesForAccelerator(string: str) -> java.util.Set[str]:
        """
        
            Parameters:
                ccdbAccelerator (java.lang.String): if null the method will be applied on accelerators.
        
            Returns:
                All existing front end names for the given CCDB accelerator.
        
        
        """
        ...
    @staticmethod
    def getPropertiesForDevice(string: str) -> java.util.Set[str]:
        """
        
            Parameters:
                deviceName (java.lang.String): The device name used to retrieve its associated properties from
        
            Returns:
                A Set of properties associated with the given device name, could be empty if the device does not exists.
        
        
        """
        ...
    @staticmethod
    def getRealColumnCount(propertyField: cern.accsoft.ccs.ccda.client.model.device.PropertyField) -> int:
        """
        
            Parameters:
                propertyField (cern.accsoft.ccs.ccda.client.model.device.PropertyField): :code:`PropertyField` to get the column count from.
        
            Returns:
                The the column count if defined or null if not defined.
        
        
        """
        ...
    @staticmethod
    def getRealRowCount(propertyField: cern.accsoft.ccs.ccda.client.model.device.PropertyField) -> int:
        """
        
            Parameters:
                propertyField (cern.accsoft.ccs.ccda.client.model.device.PropertyField): :code:`PropertyField` to get the row count from.
        
            Returns:
                The the row count if defined or null if not defined.
        
        
        """
        ...
    @staticmethod
    def getRowCount(propertyField: cern.accsoft.ccs.ccda.client.model.device.PropertyField) -> int:
        """
        
            Parameters:
                propertyField (cern.accsoft.ccs.ccda.client.model.device.PropertyField): :code:`PropertyField` to get the row count from.
        
            Returns:
                The the row count if defined or 1 if not defined.
        
        
        """
        ...
    @staticmethod
    def isCcdaEnvironmentDev() -> bool:
        """
        
            Returns:
                true if the CCDA server Environment is the DEV one, false otherwise.
        
        
        """
        ...
    @staticmethod
    def isCcdaEnvironmentPro() -> bool:
        """
        
            Returns:
                true if the CCDA server Environment is the PRO one, false otherwise.
        
        
        """
        ...
    @staticmethod
    def isDeviceValid(string: str) -> bool:
        """
            Returns the device associated with given name or alias.
        
            Parameters:
                deviceNameOrAlias (java.lang.String): The device name or alias used to retrieve the associated Device Object.
        
        
        """
        ...
    @staticmethod
    def isParameterValid(string: str) -> bool:
        """
        
            Parameters:
                parameterName (java.lang.String): the parameter name, built as device/property#field, to check for validity
        
            Returns:
                :code:`true` if parameter name is valid, or :code:`false` otherwise
        
        
        """
        ...
    @typing.overload
    @staticmethod
    def isSimpleParameter(device: cern.accsoft.ccs.ccda.client.model.device.Device, deviceClassProperty: cern.accsoft.ccs.ccda.client.model.device.DeviceClassProperty) -> bool:
        """
        public static boolean isSimpleParameter (cern.accsoft.ccs.ccda.client.model.device.Device device, cern.accsoft.ccs.ccda.client.model.device.DeviceClassProperty deviceClassProperty, boolean usePropertyDataFields)
        
        
        """
        ...
    @typing.overload
    @staticmethod
    def isSimpleParameter(device: cern.accsoft.ccs.ccda.client.model.device.Device, deviceClassProperty: cern.accsoft.ccs.ccda.client.model.device.DeviceClassProperty, boolean: bool) -> bool: ...
    @staticmethod
    def toStringArray(*string: str) -> typing.MutableSequence[str]:
        """
        
            Parameters:
                string (java.lang.String...): 
            Returns:
        
        
        """
        ...

class CcmCcdaUtils:
    """
    public class CcmCcdaUtils extends java.lang.Object
    """
    DB_WILDCARD: typing.ClassVar[str] = ...
    """
    public static final java.lang.String DB_WILDCARD
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    DEFAULT_INDENT: typing.ClassVar[str] = ...
    """
    public static final java.lang.String DEFAULT_INDENT
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    SYSPROP_FORCE_CCDB_DISABLED: typing.ClassVar[str] = ...
    """
    public static final java.lang.String SYSPROP_FORCE_CCDB_DISABLED
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    FORCE_CCDB_DISABLED: typing.ClassVar[bool] = ...
    """
    public static final boolean FORCE_CCDB_DISABLED
    
    
    """
    @staticmethod
    def cleanupApplication(ccmApplication: cern.accsoft.ccs.ccda.client.ccm.CcmApplication) -> None: ...
    @staticmethod
    def cleanupConsoleMenu(ccmConsoleMenu: cern.accsoft.ccs.ccda.client.ccm.CcmConsoleMenu) -> None: ...
    @staticmethod
    def cleanupOpConfig(ccmOpConfig: cern.accsoft.ccs.ccda.client.ccm.CcmOpConfig) -> None: ...
    @staticmethod
    def cleanupSubstitutionEntity(ccmSubstitutionEntity: cern.accsoft.ccs.ccda.client.ccm.CcmSubstitutionEntity) -> None: ...
    @staticmethod
    def cleanupTool(ccmTool: cern.accsoft.ccs.ccda.client.ccm.CcmTool) -> None: ...
    @staticmethod
    def dumpApplicWindow(ccmApplicationWindow: cern.accsoft.ccs.ccda.client.ccm.CcmApplicationWindow) -> str: ...
    @staticmethod
    def dumpApplicWindowList(list: java.util.List[cern.accsoft.ccs.ccda.client.ccm.CcmApplicationWindow]) -> str: ...
    @staticmethod
    def dumpApplication(ccmApplication: cern.accsoft.ccs.ccda.client.ccm.CcmApplication, string: str, boolean: bool) -> str:
        """
            This method allow you to get a "user friendly" toString() display of the given :code:`CcmApplication` in a generic way
            as the CCDB :code:`CcmApplication` models are only providing their name.
        
            Parameters:
                application (cern.accsoft.ccs.ccda.client.ccm.CcmApplication): The CCDB :code:`CcmApplication` model object to get its toString() information from.
                separator (java.lang.String): The :code:`CcmSubstitutionEntity` model field separator.(if null ", " will be used)
                skipTimeStamp (boolean): If true, the timestamp information will not be dumped. * @return A string representation of the given
                    :code:`CcmApplication` object.
        
        
        """
        ...
    @typing.overload
    @staticmethod
    def dumpCcdbModel(identifiable: cern.accsoft.ccs.ccda.client.ccm.Identifiable) -> str:
        """
            This generic method allow you to get a "user friendly" toString() display of the given object in a generic way as the
            CCDB :code:`Identifiable` models are only providing their name.
        
            Parameters:
                indentifiable (cern.accsoft.ccs.ccda.client.ccm.Identifiable): The CCDB :code:`Identifiable` model object to get its toString() information from.
        
            Returns:
                A string representation of the given :code:`Identifiable` object.
        
            This generic method allow you to get a "user friendly" toString() display of the given object in a generic way as the
            CCDB :code:`Identifiable` models are only providing their name.
        
            Parameters:
                indentifiable (cern.accsoft.ccs.ccda.client.ccm.Identifiable): The CCDB :code:`Identifiable` model object to get its toString() information from.
                separator (java.lang.String): The :code:`CcmSubstitutionEntity` model field separator.(if null ", " will be used) * @return A string representation of
                    the given :code:`Identifiable` object.
                skipTimeStamp (boolean): If true, the timestamp information will not be dumped.
        
        
        """
        ...
    @typing.overload
    @staticmethod
    def dumpCcdbModel(identifiable: cern.accsoft.ccs.ccda.client.ccm.Identifiable, string: str, boolean: bool) -> str: ...
    @staticmethod
    def dumpConsoleMenu(ccmConsoleMenu: cern.accsoft.ccs.ccda.client.ccm.CcmConsoleMenu, string: str, boolean: bool) -> str:
        """
            This method allow you to get a "user friendly" toString() display of the given :code:`CcmConsoleMenu` in a generic way
            as the CCDB :code:`CcmConsoleMenu` models are only providing their name.
        
            Parameters:
                consoleMenu (cern.accsoft.ccs.ccda.client.ccm.CcmConsoleMenu): The CCDB :code:`CcmConsoleMenu` model object to get its toString() information from.
                separator (java.lang.String): The :code:`CcmSubstitutionEntity` model field separator.(if null ", " will be used)
                skipTimeStamp (boolean): If true, the timestamp information will not be dumped.
        
            Returns:
                A string representation of the given :code:`CcmConsoleMenu` object.
        
        
        """
        ...
    @staticmethod
    def dumpConsolemenuTree(string: str, ccmConsoleMenu: cern.accsoft.ccs.ccda.client.ccm.CcmConsoleMenu, stringBuilder: java.lang.StringBuilder) -> str: ...
    @staticmethod
    def dumpOpconfig(ccmOpConfig: cern.accsoft.ccs.ccda.client.ccm.CcmOpConfig, string: str, boolean: bool) -> str:
        """
            This method allow you to get a "user friendly" toString() display of the given :code:`CcmOpConfig` in a generic way as
            the CCDB :code:`CcmOpConfig` models are only providing their name.
        
            Parameters:
                opConfig (cern.accsoft.ccs.ccda.client.ccm.CcmOpConfig): The CCDB :code:`CcmOpConfig` model object to get its toString() information from.
                separator (java.lang.String): The :code:`CcmSubstitutionEntity` model field separator.(if null ", " will be used)
                skipTimeStamp (boolean): If true, the time stamp information will not be dumped.
        
            Returns:
                A string representation of the given :code:`CcmOpConfig` object.
        
        
        """
        ...
    @staticmethod
    def dumpOsCommand(osName: cern.accsoft.ccs.ccda.client.ccm.enums.OsName, string: str) -> str: ...
    @staticmethod
    def dumpOsCommands(string: str, list: java.util.List[cern.accsoft.ccs.ccda.client.ccm.CcmOsCommand]) -> str: ...
    @staticmethod
    def dumpSubstitutionEntity(ccmSubstitutionEntity: cern.accsoft.ccs.ccda.client.ccm.CcmSubstitutionEntity, string: str, boolean: bool) -> str:
        """
            This method allow you to get a "user friendly" toString() display of the given :code:`CcmSubstitutionEntity` in a
            generic way as the CCDB :code:`CcmSubstitutionEntity` models are only providing their name.
        
            Parameters:
                substitutionEntity (cern.accsoft.ccs.ccda.client.ccm.CcmSubstitutionEntity): The CCDB :code:`CcmSubstitutionEntity` model object to get its toString() information from.
                separator (java.lang.String): The :code:`CcmSubstitutionEntity` model field separator.(if null ", " will be used)
                skipTimeStamp (boolean): If true, the timestamp information will not be dumped.
        
            Returns:
                A string representation of the given :code:`CcmSubstitutionEntity` object.
        
        
        """
        ...
    @staticmethod
    def dumpTool(ccmTool: cern.accsoft.ccs.ccda.client.ccm.CcmTool, string: str, boolean: bool) -> str:
        """
            This method allow you to get a "user friendly" toString() display of the given :code:`CcmTool` in a generic way as the
            CCDB :code:`CcmTool` models are only providing their name.
        
            Parameters:
                tool (cern.accsoft.ccs.ccda.client.ccm.CcmTool): The CCDB :code:`CcmTool` model object to get its toString() information from.
                separator (java.lang.String): The :code:`CcmSubstitutionEntity` model field separator.(if null ", " will be used)
                skipTimeStamp (boolean): If true, the timestamp information will not be dumped. * @return A string representation of the given :code:`CcmTool`
                    object.
        
        
        """
        ...
    @staticmethod
    def getAccelerator(string: str) -> cern.accsoft.ccs.ccda.client.model.accelerator.Accelerator:
        """
        
            Parameters:
                acceleratorName (java.lang.String): The :code:`Accelerator` to retrieve the name from.
        
            Returns:
                The :code:`Accelerator` name or null if the :code:`Accelerator` is null.
        
        
        """
        ...
    @staticmethod
    def getAcceleratorName(accelerator: cern.accsoft.ccs.ccda.client.model.accelerator.Accelerator) -> str:
        """
        
            Parameters:
                accelerator (cern.accsoft.ccs.ccda.client.model.accelerator.Accelerator): The :code:`Accelerator` to retrieve the name from.
        
            Returns:
                The :code:`Accelerator` name or null if the :code:`Accelerator` is null.
        
        
        """
        ...
    @staticmethod
    def getAcceleratorTimingDomainName(accelerator: cern.accsoft.ccs.ccda.client.model.accelerator.Accelerator) -> str:
        """
        
            Parameters:
                accelerator (cern.accsoft.ccs.ccda.client.model.accelerator.Accelerator): The :code:`Accelerator` to retrieve the :code:`TimingDomain` name from.
        
            Returns:
                The :code:`TimingDomain` name associated with the given :code:`Accelerator`, or null if not defined.
        
        
        """
        ...
    @staticmethod
    def getAccountService() -> cern.accsoft.ccs.ccda.client.accountegroup.AccountService:
        """
        
            Returns:
                The default AccountService used in this product for the :code:`Environment` specified by the "ccda.env" system property
                or :code:`Environment.PRO` if no system property is defined. @ If CCDB server is not available.
        
        
        """
        ...
    @staticmethod
    def getAccountsMatchingName(string: str) -> java.util.List[cern.accsoft.ccs.ccda.client.accountegroup.Account]:
        """
        
            Parameters:
                patternToSearch (java.lang.String): 
            Returns:
        
        
        """
        ...
    @staticmethod
    def getApplicationById(long: int) -> cern.accsoft.ccs.ccda.client.ccm.CcmApplication:
        """
        
            Parameters:
                id (long): The id used to retrieve its associated :code:`CcmApplication`.
        
            Returns:
                The :code:`CcmApplication` Object associated with the given id argument. @ In case of problem accessing the CCDB.
        
        
        """
        ...
    @staticmethod
    def getApplicationByName(string: str) -> cern.accsoft.ccs.ccda.client.ccm.CcmApplication:
        """
        
            Parameters:
                name (java.lang.String): The name used to retrieve its associated :code:`CcmApplication`.
        
            Returns:
                The :code:`CcmApplication` Object associated with the given name argument. @ In case of problem accessing the CCDB.
        
        
        """
        ...
    @staticmethod
    def getApplicationDependencies(ccmApplication: cern.accsoft.ccs.ccda.client.ccm.CcmApplication) -> java.util.List[str]:
        """
        
            Parameters:
                application (cern.accsoft.ccs.ccda.client.ccm.CcmApplication): The :code:`CcmApplication` Object used to retrieve all its CcmOpConfig CCM menus dependencies from the CCDB.
        
            Returns:
                The list of CcmOpConfig CCM menus dependencies associated with the given :code:`CcmApplication` Object. @ In case of
                problem accessing the CCDB.
        
        
        """
        ...
    @staticmethod
    def getApplications() -> java.util.List[cern.accsoft.ccs.ccda.client.ccm.CcmApplication]: ...
    @staticmethod
    def getApplicationsForOpConfig(string: str) -> java.util.List[cern.accsoft.ccs.ccda.client.ccm.CcmApplication]:
        """
        
            Parameters:
                opConfigName (java.lang.String): The Operational Configuration name used to retrieve its associated :code:`Applications`.
        
            Returns:
                The :code:`Applications` associated with the given opConfigName argument. @ In case of problem accessing the CCDB.
        
        
        """
        ...
    @staticmethod
    def getCcmConfigurationService() -> cern.accsoft.ccs.ccda.client.ccm.CcmConfigurationService:
        """
        
            Returns:
                The default CcmConfigurationService used in this product for the :code:`Environment` specified by the "ccda.env" system
                property or :code:`Environment.PRO` if no system property is defined. @ If CCDB server is not available.
        
        
        """
        ...
    @staticmethod
    def getCernAcceleratorForOpConfig(string: str) -> cern.accsoft.commons.domain.CernAccelerator:
        """
            Returns The :code:`CernAccelerator` associated with the given opConfig name.
        
        
        
            Parameters:
                opConfigName (java.lang.String): The opConfig name.
        
            Returns:
                The :code:`CernAccelerator` associated with the given opConfig name.
        
        
        """
        ...
    @staticmethod
    def getConsoleMenuById(long: int) -> cern.accsoft.ccs.ccda.client.ccm.CcmConsoleMenu:
        """
        
            Parameters:
                id (long): The id used to retrieve its associated :code:`CcmConsoleMenu`.
        
            Returns:
                The :code:`CcmConsoleMenu` Object associated with the given id argument. @ In case of problem accessing the CCDB.
        
        
        """
        ...
    @staticmethod
    def getDefaultOpConfig() -> str:
        """
        
            Returns:
                The default CcmOpConfig which is the first one of all existing OpConfigs.
        
        
        """
        ...
    @staticmethod
    def getDeviceClassNames() -> java.util.List[str]:
        """
        
            Returns:
                The list of ALL DeviceClass.
        
        
        """
        ...
    @staticmethod
    def getEgroupService() -> cern.accsoft.ccs.ccda.client.accountegroup.EgroupService:
        """
        
            Returns:
                The default EgroupService used in this product for the :code:`Environment` specified by the "ccda.env" system property
                or :code:`Environment.PRO` if no system property is defined. @ If CCDB server is not available.
        
        
        """
        ...
    @staticmethod
    def getEgroupsMatchingName(string: str) -> java.util.List[cern.accsoft.ccs.ccda.client.accountegroup.EgroupInfo]:
        """
        
            Parameters:
                patternToSearch (java.lang.String): 
            Returns:
        
        
        """
        ...
    @staticmethod
    def getMatchingResponsibles(string: str) -> java.util.List['Responsible']: ...
    @staticmethod
    def getName(named: cern.accsoft.commons.util.Named) -> str:
        """
        
            Parameters:
                named (cern.accsoft.commons.util.Named): The :code:`Named` to retrieve the name from.
        
            Returns:
                The :code:`Accelerator` name or null if the :code:`Named` is null.
        
        
        """
        ...
    @staticmethod
    def getOpConfig(string: str) -> cern.accsoft.ccs.ccda.client.ccm.CcmOpConfig: ...
    @staticmethod
    def getOpConfigNames() -> java.util.List[str]: ...
    @staticmethod
    def getOpConfigs() -> java.util.List[cern.accsoft.ccs.ccda.client.ccm.CcmOpConfig]: ...
    @staticmethod
    def getProgramDefinition(string: str) -> 'ProgramDefinition':
        """
            Returns the ProgramDefinition according to the programName given in parameter.
        
            Parameters:
                programName (java.lang.String): the programName to look program definitions for.
        
            Returns:
                the ProgramDefinition array according to the programName given in parameter.
        
        
        """
        ...
    @staticmethod
    def getProgramDefinitions(string: str, string2: str) -> java.util.Set['ProgramDefinition']: ...
    @staticmethod
    def getRbacRoleForOpConfigName(string: str) -> str: ...
    @staticmethod
    def getRbacRoles() -> java.util.List[str]:
        """
        
            Returns:
                All existing RBAC access roles related to CCM edition operations. @ In case of problem accessing the CCDB.
        
        
        """
        ...
    @staticmethod
    def getResponsiblesByNameIn(set: java.util.Set[str]) -> java.util.List['Responsible']: ...
    @staticmethod
    def getRootConsoleMenuForOpConfig(string: str) -> cern.accsoft.ccs.ccda.client.ccm.CcmConsoleMenu: ...
    @staticmethod
    def getSubstitutionDependencies(ccmSubstitutionEntity: cern.accsoft.ccs.ccda.client.ccm.CcmSubstitutionEntity) -> java.util.List[str]: ...
    @staticmethod
    def getSubstitutionEntities() -> java.util.List[cern.accsoft.ccs.ccda.client.ccm.CcmSubstitutionEntity]: ...
    @staticmethod
    def getSubstitutionEntityById(long: int) -> cern.accsoft.ccs.ccda.client.ccm.CcmSubstitutionEntity:
        """
        
            Parameters:
                id (long): The id used to retrieve its associated :code:`CcmSubstitutionEntity`.
        
            Returns:
                The :code:`CcmSubstitutionEntity` Object associated with the given id argument. @ In case of problem accessing the CCDB.
        
        
        """
        ...
    @staticmethod
    def getToolById(long: int) -> cern.accsoft.ccs.ccda.client.ccm.CcmTool:
        """
        
            Parameters:
                id (long): The id used to retrieve its associated :code:`CcmTool`.
        
            Returns:
                The :code:`CcmTool` Object associated with the given id argument. @ In case of problem accessing the CCDB.
        
        
        """
        ...
    @staticmethod
    def getToolDependencies(ccmTool: cern.accsoft.ccs.ccda.client.ccm.CcmTool) -> java.util.List[str]: ...
    @staticmethod
    def getTools() -> java.util.List[cern.accsoft.ccs.ccda.client.ccm.CcmTool]: ...
    @staticmethod
    def isCcmServiceAvailable() -> bool:
        """
        
            Returns:
                true if the CCM CCS service is available, false otherwise.
        
        
        """
        ...
    @staticmethod
    def isForceDisableCcmServiceForDebugPurpose() -> bool: ...
    @staticmethod
    def isValidOpConfig(string: str) -> bool:
        """
        
            Parameters:
                opConfigName (java.lang.String): The Operational Configuration name to check its validity.
        
            Returns:
                true if the given Operational Configuration name argument is existing, false otherwise. @ In case of problem accessing
                the CCDB.
        
        
        """
        ...
    @staticmethod
    def setForceDisableCcmServiceForDebugPurpose(boolean: bool) -> None: ...

class CcmCcdaUtilsJSon:
    """
    public class CcmCcdaUtilsJSon extends java.lang.Object
    """
    LOCAL_FILE_PATH_PREFIX: typing.ClassVar[str] = ...
    """
    public static final java.lang.String LOCAL_FILE_PATH_PREFIX
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    LOCAL_FILE_EXTENSION: typing.ClassVar[str] = ...
    """
    public static final java.lang.String LOCAL_FILE_EXTENSION
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    LOCAL_FILE_DB_PREFIX: typing.ClassVar[str] = ...
    """
    public static final java.lang.String LOCAL_FILE_DB_PREFIX
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    LOCAL_FILE_APPLICATIONS: typing.ClassVar[str] = ...
    """
    public static final java.lang.String LOCAL_FILE_APPLICATIONS
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    LOCAL_FILE_TOOLS: typing.ClassVar[str] = ...
    """
    public static final java.lang.String LOCAL_FILE_TOOLS
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    LOCAL_FILE_SUBST: typing.ClassVar[str] = ...
    """
    public static final java.lang.String LOCAL_FILE_SUBST
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    LOCAL_FILE_RESPONSIBLES: typing.ClassVar[str] = ...
    """
    public static final java.lang.String LOCAL_FILE_RESPONSIBLES
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    LOCAL_FILE_OPCONFIGS: typing.ClassVar[str] = ...
    """
    public static final java.lang.String LOCAL_FILE_OPCONFIGS
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    @staticmethod
    def clearCache() -> None: ...
    @staticmethod
    def createFileAndSetAccessToAllUsers(string: str) -> java.io.File: ...
    @staticmethod
    def getSubstitutionEntities() -> java.util.List[cern.accsoft.ccs.ccda.client.ccm.CcmSubstitutionEntity]: ...
    @staticmethod
    def saveCommonFiles() -> None: ...
    @staticmethod
    def saveRootConsoleMenuForOpConfig(string: str) -> None:
        """
            Save All existing ConsoleMenus to a local file. @ In case of problem accessing the CCDB.
        
        """
        ...

class CcsException(java.lang.Exception):
    """
    public class CcsException extends java.lang.Exception
    
        This is a CCS extension of the :code:`Exception`
    
        Also see:
            :meth:`~serialized`
    """
    @typing.overload
    def __init__(self, string: str): ...
    @typing.overload
    def __init__(self, string: str, throwable: java.lang.Throwable): ...
    @typing.overload
    def __init__(self, throwable: java.lang.Throwable): ...

class ConvertCcsToJapcMeaning:
    """
    public final class ConvertCcsToJapcMeaning extends java.lang.Object
    
        Convert CCS :code:`FieldValueMeaning` to a JAPC :code:`SimpleValueStandardMeaning`.
    """
    @staticmethod
    def ccsToJapcMeaning(fieldValueMeaning: cern.accsoft.ccs.ccda.client.model.device.FieldValueMeaning) -> cern.japc.value.SimpleValueStandardMeaning:
        """
            Convert the given CCS :code:`FieldValueMeaning` to a JAPC :code:`SimpleValueStandardMeaning`.
        
        
        
            Parameters:
                fieldValueMeaning (cern.accsoft.ccs.ccda.client.model.device.FieldValueMeaning): The CCS :code:`FieldValueMeaning`.
        
            Returns:
                the associated JAPC :code:`SimpleValueStandardMeaning` if the given :code:`FieldValueMeaning` is valid.
        
            Raises:
                java.lang.IllegalArgumentException: if the given :code:`FieldValueMeaning` is NOT valid.
        
        
        """
        ...

class ConvertCcsToJapcValueType:
    """
    public final class ConvertCcsToJapcValueType extends java.lang.Object
    
        Convert CCS type and primitive data type pair Strings to proper JAPC :code:`ValueType`.
    
    
        Please see https://issues.cern.ch/browse/CCS-9354 for more details.
    """
    SCALAR: typing.ClassVar[str] = ...
    """
    public static final java.lang.String SCALAR
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    ARRAY: typing.ClassVar[str] = ...
    """
    public static final java.lang.String ARRAY
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    ARRAY2D: typing.ClassVar[str] = ...
    """
    public static final java.lang.String ARRAY2D
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    ENUM: typing.ClassVar[str] = ...
    """
    public static final java.lang.String ENUM
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    ENUM_ARRAY: typing.ClassVar[str] = ...
    """
    public static final java.lang.String ENUM_ARRAY
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    ENUM_ARRAY2D: typing.ClassVar[str] = ...
    """
    public static final java.lang.String ENUM_ARRAY2D
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    BIT_ENUM: typing.ClassVar[str] = ...
    """
    public static final java.lang.String BIT_ENUM
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    BIT_ENUM_ARRAY: typing.ClassVar[str] = ...
    """
    public static final java.lang.String BIT_ENUM_ARRAY
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    BIT_ENUM_ARRAY2D: typing.ClassVar[str] = ...
    """
    public static final java.lang.String BIT_ENUM_ARRAY2D
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    DIAG_FWK_TOPIC: typing.ClassVar[str] = ...
    """
    public static final java.lang.String DIAG_FWK_TOPIC
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    DIAG_CUSTOM_TOPIC: typing.ClassVar[str] = ...
    """
    public static final java.lang.String DIAG_CUSTOM_TOPIC
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    NOTIFICATION_UPDATE_ENUM: typing.ClassVar[str] = ...
    """
    public static final java.lang.String NOTIFICATION_UPDATE_ENUM
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    CUSTOM_TYPE_SCALAR: typing.ClassVar[str] = ...
    """
    public static final java.lang.String CUSTOM_TYPE_SCALAR
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    FAULT_SEVERITY: typing.ClassVar[str] = ...
    """
    public static final java.lang.String FAULT_SEVERITY
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    CONST_UINT: typing.ClassVar[str] = ...
    """
    public static final java.lang.String CONST_UINT
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    @staticmethod
    def ccsToJapcType(string: str, primitiveDataType: cern.accsoft.ccs.ccda.client.model.device.PrimitiveDataType) -> cern.japc.value.ValueType:
        """
            Convert the given CCS type and primitiveDataType Strings to a JAPC :code:`ValueType`.
        
        
            // see https://issues.cern.ch/browse/CCS-9354 for more details.
        
            Parameters:
                type (java.lang.String): The CCS type as String.
                primitiveDataType (cern.accsoft.ccs.ccda.client.model.device.PrimitiveDataType): The CCS primitive data type.
        
            Returns:
                the associated JAPC :code:`ValueType` if the given type and primitiveDataType are valid.
        
            Raises:
                java.lang.IllegalArgumentException: if the given type and primitiveDataType are NOT valid.
        
        
        """
        ...

class ConvertPrimitiveDataTypeTypeToEnumTypeBitSize:
    """
    public final class ConvertPrimitiveDataTypeTypeToEnumTypeBitSize extends java.lang.Object
    
        Convert the given :code:`ValueType` to a :code:`EnumTypeBitSize`
    """
    @staticmethod
    def primitiveDataTypeToEnumTypeBitSize(primitiveDataType: cern.accsoft.ccs.ccda.client.model.device.PrimitiveDataType) -> cern.japc.value.EnumTypeBitSize:
        """
            Convert the given :code:`PrimitiveDataType` to a :code:`EnumTypeBitSize`.
        
        
        
            Parameters:
                primitiveDataType (cern.accsoft.ccs.ccda.client.model.device.PrimitiveDataType): type to be converted
        
            Returns:
                the associated :code:`EnumTypeBitSize` if the given :code:`ValueType` is valid.
        
            Raises:
                java.lang.IllegalArgumentException: if the given :code:`ValueType` is NOT valid.
        
        
        """
        ...

class ProgramDefinition:
    """
    public interface ProgramDefinition
    """
    def getApplicCommand(self) -> str:
        """
            Gets the basic command to start the application, or null if none.
        
        
        
        """
        ...
    def getApplicName(self) -> str:
        """
            Gets the name of the application, or an empty String. if none.
        
        """
        ...
    def getArguments(self) -> str:
        """
            Gets the arguments for starting this instance of the application, or an empty String. if none.
        
        """
        ...
    def getBackgroundColorString(self) -> str:
        """
            Gets the background colour string of a menu button as entered in the database, else empty string. Note: if the string
            conforms to the color enumeration in the Oracle form, then this color is automatically set and this method, for setting
            the color yourself is not necessary.
        
        """
        ...
    def getDescription(self) -> str:
        """
            Gets the description of the program called from the menu item, or "-".
        
        """
        ...
    def getDeviceClass(self) -> str:
        """
        
            Returns:
                the device class for which this program is valid.
        
        
        """
        ...
    def getIconName(self) -> str:
        """
            Gets the icon name, or an empty String., for display in the menu item.
        
        """
        ...
    def getItemCategory(self) -> str:
        """
            Return item category (default an empty String.).
        
        """
        ...
    def getLabel(self) -> str:
        """
            Gets the label, for display in the menu item, or an empty String..
        
        """
        ...
    def getOsCommand(self) -> str:
        """
            Return the default command, else the operating-system dependent command, else null.
        
        """
        ...
    def getToolExe(self) -> str:
        """
            Gets the full command, including arguments if any, to start the application in the current operating system.
        
        """
        ...
    def getToolName(self) -> str:
        """
            Gets the name of the tool for starting the application (or empty string).
        
        """
        ...
    def getToolTipText(self) -> str:
        """
            Gets the tooltip text for the menu item.
        
        """
        ...
    def getWindowCount(self) -> int:
        """
            Gets the number of Windows definitions associated with this Programe definition.
        
        """
        ...
    def getWindowGeometry(self, int: int) -> str:
        """
            Gets the geometry of the main window in format XSIZExYSIZE+XPOS+YPOS, or an empty String..
        
        """
        ...
    def getWindowTitle(self, int: int) -> str:
        """
            Gets the title to be displayed on the main window of the program, or an empty String.
        
        """
        ...
    def isAutoStart(self) -> bool:
        """
            Program should be started automatically, when true.
        
        """
        ...
    def isContextIndependent(self) -> bool:
        """
            Program should appear in all console manager contexts, when true.
        
        """
        ...
    def isDeviceDependent(self) -> bool:
        """
            Program requires at least one device name as input, when true.
        
        """
        ...
    def isDevicesDependent(self) -> bool:
        """
            Program requires more than one device name as input, when true.
        
        """
        ...
    def isMultiStart(self) -> bool:
        """
            Program may be started multiple times with same parameters, when true.
        
        """
        ...

class Responsible(cern.accsoft.commons.util.Named):
    """
    public class Responsible extends java.lang.Object implements cern.accsoft.commons.util.Named
    
        This class is a wrapper for the Egroup and Account classes which are not implementing the Named interface.
    """
    @typing.overload
    def __init__(self, account: cern.accsoft.ccs.ccda.client.accountegroup.Account): ...
    @typing.overload
    def __init__(self, egroupInfo: cern.accsoft.ccs.ccda.client.accountegroup.EgroupInfo): ...
    def equals(self, object: typing.Any) -> bool:
        """
        
            Overrides:
                :code:`equals` in class :code:`java.lang.Object`
        
        
        """
        ...
    def getAccount(self) -> cern.accsoft.ccs.ccda.client.accountegroup.Account: ...
    def getEgroup(self) -> cern.accsoft.ccs.ccda.client.accountegroup.EgroupInfo: ...
    def getName(self) -> str:
        """
        
            Specified by:
                :code:`getName` in interface :code:`cern.accsoft.commons.util.Named`
        
        
        """
        ...
    def hashCode(self) -> int:
        """
        
            Overrides:
                :code:`hashCode` in class :code:`java.lang.Object`
        
        
        """
        ...
    def toString(self) -> str:
        """
        
            Overrides:
                :code:`toString` in class :code:`java.lang.Object`
        
        
        """
        ...

class ProgramDefinitionImpl(ProgramDefinition, java.io.Serializable):
    """
    public class ProgramDefinitionImpl extends java.lang.Object implements :class:`~cern.accsoft.commons.ccs.ProgramDefinition`, java.io.Serializable
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self): ...
    def getApplicCommand(self) -> str:
        """
            Gets the basic command to start the application, or null if none.
        
        
            It is not clear what was the purpose of this stuff. I guess it was there only for XMitif CCM.
        
            Specified by:
                :meth:`~cern.accsoft.commons.ccs.ProgramDefinition.getApplicCommand`Â in
                interfaceÂ :class:`~cern.accsoft.commons.ccs.ProgramDefinition`
        
            Also see:
                :meth:`~cern.accsoft.commons.ccs.ProgramDefinitionImpl.getOsCommand`
        
        
        """
        ...
    def getApplicName(self) -> str:
        """
            Gets the name of the application, or StringUtils.EMPTY_STRING if none.
        
            Specified by:
                :meth:`~cern.accsoft.commons.ccs.ProgramDefinition.getApplicName`Â in
                interfaceÂ :class:`~cern.accsoft.commons.ccs.ProgramDefinition`
        
        
        """
        ...
    def getArguments(self) -> str:
        """
            Gets the arguments for starting this instance of the application, or StringUtils.EMPTY_STRING if none. Note:
            getFullCommand(String osname) is probably all you need.
        
            Specified by:
                :meth:`~cern.accsoft.commons.ccs.ProgramDefinition.getArguments`Â in
                interfaceÂ :class:`~cern.accsoft.commons.ccs.ProgramDefinition`
        
        
        """
        ...
    def getBackgroundColorString(self) -> str:
        """
            Gets the background colour string of a menu button as entered in the database, else empty string. Note: if the string
            conforms to the color enumeration in the Oracle form, then this color is automatically set and this method, for setting
            the color yourself is not necessary.
        
            Specified by:
                :meth:`~cern.accsoft.commons.ccs.ProgramDefinition.getBackgroundColorString`Â in
                interfaceÂ :class:`~cern.accsoft.commons.ccs.ProgramDefinition`
        
        
        """
        ...
    def getDescription(self) -> str:
        """
            Gets the description of the program called from the menu item, or "-".
        
            Specified by:
                :meth:`~cern.accsoft.commons.ccs.ProgramDefinition.getDescription`Â in
                interfaceÂ :class:`~cern.accsoft.commons.ccs.ProgramDefinition`
        
        
        """
        ...
    def getDeviceClass(self) -> str:
        """
        
            Specified by:
                :meth:`~cern.accsoft.commons.ccs.ProgramDefinition.getDeviceClass`Â in
                interfaceÂ :class:`~cern.accsoft.commons.ccs.ProgramDefinition`
        
            Returns:
                the device class for which this program is valid.
        
        
        """
        ...
    def getIconName(self) -> str:
        """
            Gets the icon name, or StringUtils.EMPTY_STRING, for display in the menu item.
        
            Specified by:
                :meth:`~cern.accsoft.commons.ccs.ProgramDefinition.getIconName`Â in
                interfaceÂ :class:`~cern.accsoft.commons.ccs.ProgramDefinition`
        
        
        """
        ...
    def getItemCategory(self) -> str:
        """
            Return item category (default StringUtils.EMPTY_STRING).
        
            Specified by:
                :meth:`~cern.accsoft.commons.ccs.ProgramDefinition.getItemCategory`Â in
                interfaceÂ :class:`~cern.accsoft.commons.ccs.ProgramDefinition`
        
        
        """
        ...
    def getLabel(self) -> str:
        """
            Gets the label, for display in the menu item, or StringUtils.EMPTY_STRING.
        
            Specified by:
                :meth:`~cern.accsoft.commons.ccs.ProgramDefinition.getLabel`Â in
                interfaceÂ :class:`~cern.accsoft.commons.ccs.ProgramDefinition`
        
        
        """
        ...
    def getOsCommand(self) -> str:
        """
            Return the default command, else the operating-system dependent command, else null.
        
            Specified by:
                :meth:`~cern.accsoft.commons.ccs.ProgramDefinition.getOsCommand`Â in
                interfaceÂ :class:`~cern.accsoft.commons.ccs.ProgramDefinition`
        
            Also see:
                :meth:`~cern.accsoft.commons.ccs.ProgramDefinitionImpl.getApplicCommand`
        
        
        """
        ...
    def getToolExe(self) -> str:
        """
            Gets the full command, including arguments if any, to start the application in the current operating system.
        
            Specified by:
                :meth:`~cern.accsoft.commons.ccs.ProgramDefinition.getToolExe`Â in
                interfaceÂ :class:`~cern.accsoft.commons.ccs.ProgramDefinition`
        
        
        """
        ...
    def getToolName(self) -> str:
        """
            Gets the name of the tool for starting the application (or empty string).
        
            Specified by:
                :meth:`~cern.accsoft.commons.ccs.ProgramDefinition.getToolName`Â in
                interfaceÂ :class:`~cern.accsoft.commons.ccs.ProgramDefinition`
        
        
        """
        ...
    def getToolTipText(self) -> str:
        """
            Gets the tooltip text for the menu item.
        
            Specified by:
                :meth:`~cern.accsoft.commons.ccs.ProgramDefinition.getToolTipText`Â in
                interfaceÂ :class:`~cern.accsoft.commons.ccs.ProgramDefinition`
        
        
        """
        ...
    def getWindowCount(self) -> int:
        """
            Gets the number of Windows definitions associated with this Programe definition.
        
            Specified by:
                :meth:`~cern.accsoft.commons.ccs.ProgramDefinition.getWindowCount`Â in
                interfaceÂ :class:`~cern.accsoft.commons.ccs.ProgramDefinition`
        
        
        """
        ...
    def getWindowGeometry(self, int: int) -> str:
        """
            Gets the geometry of the main window in format XSIZExYSIZE+XPOS+YPOS, or StringUtils.EMPTY_STRING.
        
            Specified by:
                :meth:`~cern.accsoft.commons.ccs.ProgramDefinition.getWindowGeometry`Â in
                interfaceÂ :class:`~cern.accsoft.commons.ccs.ProgramDefinition`
        
        
        """
        ...
    def getWindowTitle(self, int: int) -> str:
        """
            Gets the title to be displayed on the main window of the program, or StringUtils.EMPTY_STRING.
        
            Specified by:
                :meth:`~cern.accsoft.commons.ccs.ProgramDefinition.getWindowTitle`Â in
                interfaceÂ :class:`~cern.accsoft.commons.ccs.ProgramDefinition`
        
        
        """
        ...
    def isAutoStart(self) -> bool:
        """
            Program should be started automatically, when true.
        
            Specified by:
                :meth:`~cern.accsoft.commons.ccs.ProgramDefinition.isAutoStart`Â in
                interfaceÂ :class:`~cern.accsoft.commons.ccs.ProgramDefinition`
        
        
        """
        ...
    def isContextIndependent(self) -> bool:
        """
            Program should appear in all console manager contexts, when true.
        
            Specified by:
                :meth:`~cern.accsoft.commons.ccs.ProgramDefinition.isContextIndependent`Â in
                interfaceÂ :class:`~cern.accsoft.commons.ccs.ProgramDefinition`
        
        
        """
        ...
    def isDeviceDependent(self) -> bool:
        """
            Program requires at least one device name as input, when true.
        
            Specified by:
                :meth:`~cern.accsoft.commons.ccs.ProgramDefinition.isDeviceDependent`Â in
                interfaceÂ :class:`~cern.accsoft.commons.ccs.ProgramDefinition`
        
        
        """
        ...
    def isDevicesDependent(self) -> bool:
        """
            Program requires more than one device name as input, when true.
        
            Specified by:
                :meth:`~cern.accsoft.commons.ccs.ProgramDefinition.isDevicesDependent`Â in
                interfaceÂ :class:`~cern.accsoft.commons.ccs.ProgramDefinition`
        
        
        """
        ...
    def isMultiStart(self) -> bool:
        """
            Program may be started multiple times with same parameters, when true.
        
            Specified by:
                :meth:`~cern.accsoft.commons.ccs.ProgramDefinition.isMultiStart`Â in
                interfaceÂ :class:`~cern.accsoft.commons.ccs.ProgramDefinition`
        
        
        """
        ...
    def setApplicCommand(self, string: str) -> None: ...
    def setApplicDescrip(self, string: str) -> None: ...
    def setApplicName(self, string: str) -> None: ...
    def setArguments(self, string: str) -> None: ...
    def setAutoStartFlag(self, boolean: bool) -> None: ...
    def setBgColor(self, string: str) -> None: ...
    def setDeviceClass(self, string: str) -> None: ...
    def setEqnamFlag(self, boolean: bool) -> None: ...
    def setIconName(self, string: str) -> None: ...
    def setIndepFlag(self, boolean: bool) -> None: ...
    def setItemCategory(self, string: str) -> None: ...
    def setItemLabel(self, string: str) -> None: ...
    def setMultiEqnamFlag(self, boolean: bool) -> None: ...
    def setMultiStartFlag(self, boolean: bool) -> None: ...
    def setOsCommands(self, map: typing.Union[java.util.Map[str, str], typing.Mapping[str, str]]) -> None: ...
    def setOsToolExes(self, map: typing.Union[java.util.Map[str, str], typing.Mapping[str, str]]) -> None: ...
    def setToolName(self, string: str) -> None: ...
    def setWinGeoms(self, map: typing.Union[java.util.Map[int, str], typing.Mapping[int, str]]) -> None: ...
    def setWinTitles(self, map: typing.Union[java.util.Map[int, str], typing.Mapping[int, str]]) -> None: ...
    def toString(self) -> str:
        """
            Returns a String definition of this object
        
            Overrides:
                :code:`toString` in class :code:`java.lang.Object`
        
        
        """
        ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.ccs")``.

    AcceleratorInfo: typing.Type[AcceleratorInfo]
    CcdaAccess: typing.Type[CcdaAccess]
    CcdaUtils: typing.Type[CcdaUtils]
    CcmCcdaUtils: typing.Type[CcmCcdaUtils]
    CcmCcdaUtilsJSon: typing.Type[CcmCcdaUtilsJSon]
    CcsException: typing.Type[CcsException]
    ConvertCcsToJapcMeaning: typing.Type[ConvertCcsToJapcMeaning]
    ConvertCcsToJapcValueType: typing.Type[ConvertCcsToJapcValueType]
    ConvertPrimitiveDataTypeTypeToEnumTypeBitSize: typing.Type[ConvertPrimitiveDataTypeTypeToEnumTypeBitSize]
    ProgramDefinition: typing.Type[ProgramDefinition]
    ProgramDefinitionImpl: typing.Type[ProgramDefinitionImpl]
    Responsible: typing.Type[Responsible]
