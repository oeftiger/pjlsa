
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import java.lang.reflect
import java.util.concurrent
import jpype
import typing



class MethodInvoker:
    def invoke(self, objectArray: typing.Union[typing.List[typing.Any], jpype.JArray], method: java.lang.reflect.Method, objectArray2: typing.Union[typing.List[typing.Any], jpype.JArray]) -> typing.MutableSequence[typing.Any]: ...

class AbstractMethodInvoker(MethodInvoker):
    def __init__(self): ...
    def invoke(self, objectArray: typing.Union[typing.List[typing.Any], jpype.JArray], method: java.lang.reflect.Method, objectArray2: typing.Union[typing.List[typing.Any], jpype.JArray]) -> typing.MutableSequence[typing.Any]: ...

class ExecutorServiceMethodInvoker(AbstractMethodInvoker):
    @typing.overload
    def __init__(self): ...
    @typing.overload
    def __init__(self, executorService: java.util.concurrent.ExecutorService, long: int): ...
    def getExecutorService(self) -> java.util.concurrent.ExecutorService: ...
    def getMethodInvocationTimeout(self) -> int: ...

class SameThreadMethodInvoker(AbstractMethodInvoker):
    @typing.overload
    def __init__(self): ...
    @typing.overload
    def __init__(self, boolean: bool): ...

class SwingThreadMethodInvoker(AbstractMethodInvoker):
    def __init__(self): ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.util.reflect")``.

    AbstractMethodInvoker: typing.Type[AbstractMethodInvoker]
    ExecutorServiceMethodInvoker: typing.Type[ExecutorServiceMethodInvoker]
    MethodInvoker: typing.Type[MethodInvoker]
    SameThreadMethodInvoker: typing.Type[SameThreadMethodInvoker]
    SwingThreadMethodInvoker: typing.Type[SwingThreadMethodInvoker]
