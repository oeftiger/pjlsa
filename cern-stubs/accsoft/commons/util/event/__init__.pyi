
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern
import cern.accsoft.commons.util.reflect
import java.awt.event
import java.beans
import java.lang
import java.util
import javax.swing.event
import jpype
import typing



_EventListenerSupport__T = typing.TypeVar('_EventListenerSupport__T')  # <T>
class EventListenerSupport(typing.Generic[_EventListenerSupport__T]):
    @typing.overload
    def __init__(self, class_: typing.Type[_EventListenerSupport__T]): ...
    @typing.overload
    def __init__(self, class_: typing.Type[_EventListenerSupport__T], methodInvoker: cern.accsoft.commons.util.reflect.MethodInvoker): ...
    def add(self, t: _EventListenerSupport__T) -> None: ...
    def addAll(self, collection: typing.Union[java.util.Collection[_EventListenerSupport__T], typing.Sequence[_EventListenerSupport__T], typing.Set[_EventListenerSupport__T]]) -> None: ...
    def addAllWeakly(self, collection: typing.Union[java.util.Collection[_EventListenerSupport__T], typing.Sequence[_EventListenerSupport__T], typing.Set[_EventListenerSupport__T]]) -> None: ...
    def addWeakly(self, t: _EventListenerSupport__T) -> None: ...
    def clear(self) -> None: ...
    def getDispatcher(self) -> _EventListenerSupport__T: ...
    def getListenerInvoker(self) -> cern.accsoft.commons.util.reflect.MethodInvoker: ...
    def getListeners(self) -> java.util.List[_EventListenerSupport__T]: ...
    _newInstance__T = typing.TypeVar('_newInstance__T')  # <T>
    @staticmethod
    def newInstance(class_: typing.Type[_newInstance__T]) -> 'EventListenerSupport'[_newInstance__T]: ...
    def remove(self, t: _EventListenerSupport__T) -> None: ...
    def removeAll(self, collection: typing.Union[java.util.Collection[_EventListenerSupport__T], typing.Sequence[_EventListenerSupport__T], typing.Set[_EventListenerSupport__T]]) -> None: ...

class EventListeners:
    def __init__(self): ...
    @staticmethod
    def actionListener(object: typing.Any, string: str) -> java.awt.event.ActionListener: ...
    @staticmethod
    def changeListener(object: typing.Any, string: str) -> javax.swing.event.ChangeListener: ...
    _create_0__T = typing.TypeVar('_create_0__T')  # <T>
    _create_1__T = typing.TypeVar('_create_1__T')  # <T>
    @typing.overload
    @staticmethod
    def create(class_: typing.Type[_create_0__T], object: typing.Any, string: str) -> _create_0__T: ...
    @typing.overload
    @staticmethod
    def create(class_: typing.Type[_create_1__T], object: typing.Any, string: str, string2: str) -> _create_1__T: ...
    @staticmethod
    def listSelectionListener(object: typing.Any, string: str) -> javax.swing.event.ListSelectionListener: ...
    @staticmethod
    def propertyChangeListener(object: typing.Any, string: str) -> java.beans.PropertyChangeListener: ...

_SyncEventListenerSupport__T = typing.TypeVar('_SyncEventListenerSupport__T')  # <T>
class SyncEventListenerSupport(EventListenerSupport[_SyncEventListenerSupport__T], typing.Generic[_SyncEventListenerSupport__T]):
    @typing.overload
    def __init__(self, class_: typing.Type[_SyncEventListenerSupport__T]): ...
    @typing.overload
    def __init__(self, class_: typing.Type[_SyncEventListenerSupport__T], methodInvoker: cern.accsoft.commons.util.reflect.MethodInvoker): ...
    def clear(self) -> None: ...
    def getListeners(self) -> java.util.List[_SyncEventListenerSupport__T]: ...
    _newInstance_0__T = typing.TypeVar('_newInstance_0__T')  # <T>
    _newInstance_1__T = typing.TypeVar('_newInstance_1__T')  # <T>
    @typing.overload
    @staticmethod
    def newInstance(class_: typing.Type[_newInstance_0__T]) -> EventListenerSupport[_newInstance_0__T]: ...
    @typing.overload
    @staticmethod
    def newInstance(class_: typing.Type[_newInstance_1__T]) -> 'SyncEventListenerSupport'[_newInstance_1__T]: ...
    def remove(self, t: _SyncEventListenerSupport__T) -> None: ...

class EventListenerSupportDemo:
    def __init__(self): ...
    @staticmethod
    def main(stringArray: typing.Union[typing.List[str], jpype.JArray]) -> None: ...
    def updateListeners(self) -> None: ...
    class MyListenerImpl(cern.accsoft.commons.util.event.EventListenerSupportDemo.MyListener, cern.accsoft.commons.util.event.EventListenerSupportDemo.ParameterValueListener):
        def __init__(self, eventListenerSupportDemo: 'EventListenerSupportDemo', string: str): ...
        def exceptionOccured(self, string: str, exception: java.lang.Exception) -> None: ...
        def sendUpdate(self, long: int) -> None: ...
        def valueReceived(self, string: str) -> None: ...
    class MyListener: ...
    class ParameterValueListener: ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.util.event")``.

    EventListenerSupport: typing.Type[EventListenerSupport]
    EventListenerSupportDemo: typing.Type[EventListenerSupportDemo]
    EventListeners: typing.Type[EventListeners]
    SyncEventListenerSupport: typing.Type[SyncEventListenerSupport]
