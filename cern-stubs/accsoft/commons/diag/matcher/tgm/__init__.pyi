
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.accsoft.commons.diag.matcher
import typing



class TgmThrowableMatcher(cern.accsoft.commons.diag.matcher.StringThrowableMatcher):
    """
    public class TgmThrowableMatcher extends :class:`~cern.accsoft.commons.diag.matcher.StringThrowableMatcher`
    
        TGM exception string-based matcher.
    """
    TGM_PROBLEM_DOMAIN: typing.ClassVar[str] = ...
    """
    public static final java.lang.String TGM_PROBLEM_DOMAIN
    
        TGM problem domain
    
        Also see:
            :meth:`~constant`
    
    
    """
    def __init__(self): ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.diag.matcher.tgm")``.

    TgmThrowableMatcher: typing.Type[TgmThrowableMatcher]
