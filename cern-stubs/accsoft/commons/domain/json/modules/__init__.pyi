
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.accsoft.commons.domain
import cern.accsoft.commons.domain.json.modules.cern
import cern.accsoft.commons.domain.particletransfers
import cern.accsoft.commons.util
import com.fasterxml.jackson.databind.module
import java.util
import java.util.function
import typing



_AbstractEnumEmulatorModule__T = typing.TypeVar('_AbstractEnumEmulatorModule__T', bound=cern.accsoft.commons.util.Named)  # <T>
class AbstractEnumEmulatorModule(com.fasterxml.jackson.databind.module.SimpleModule, typing.Generic[_AbstractEnumEmulatorModule__T]):
    """
    public abstract class AbstractEnumEmulatorModule<T extends cern.accsoft.commons.util.Named> extends com.fasterxml.jackson.databind.module.SimpleModule
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self, class_: typing.Type[_AbstractEnumEmulatorModule__T], collection: typing.Union[java.util.Collection[typing.Type[_AbstractEnumEmulatorModule__T]], typing.Sequence[typing.Type[_AbstractEnumEmulatorModule__T]]], function: typing.Union[java.util.function.Function[str, _AbstractEnumEmulatorModule__T], typing.Callable[[str], _AbstractEnumEmulatorModule__T]]): ...

class AccsoftCommonsDomainModule(com.fasterxml.jackson.databind.module.SimpleModule):
    """
    public class AccsoftCommonsDomainModule extends com.fasterxml.jackson.databind.module.SimpleModule
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self): ...
    def getDependencies(self) -> java.util.List[com.fasterxml.jackson.databind.module.SimpleModule]:
        """
        
            Overrides:
                :code:`getDependencies` in class :code:`com.fasterxml.jackson.databind.Module`
        
        
        """
        ...

class ParticleTransferTypeModule(AbstractEnumEmulatorModule[cern.accsoft.commons.domain.particletransfers.ParticleTransferType]):
    """
    public class ParticleTransferTypeModule extends :class:`~cern.accsoft.commons.domain.json.modules.AbstractEnumEmulatorModule`<cern.accsoft.commons.domain.particletransfers.ParticleTransferType>
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self): ...

class ParticleTypeModule(AbstractEnumEmulatorModule[cern.accsoft.commons.domain.ParticleType]):
    """
    public class ParticleTypeModule extends :class:`~cern.accsoft.commons.domain.json.modules.AbstractEnumEmulatorModule`<cern.accsoft.commons.domain.ParticleType>
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self): ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.domain.json.modules")``.

    AbstractEnumEmulatorModule: typing.Type[AbstractEnumEmulatorModule]
    AccsoftCommonsDomainModule: typing.Type[AccsoftCommonsDomainModule]
    ParticleTransferTypeModule: typing.Type[ParticleTransferTypeModule]
    ParticleTypeModule: typing.Type[ParticleTypeModule]
    cern: cern.accsoft.commons.domain.json.modules.cern.__module_protocol__
