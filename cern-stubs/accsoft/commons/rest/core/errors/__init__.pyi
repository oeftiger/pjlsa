
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import java.lang
import java.time
import java.util
import typing



class ErrorDto:
    """
    @Immutable public interface ErrorDto
    
        ErrorDto
    """
    @staticmethod
    def builder() -> 'DefaultErrorDto.Builder': ...
    @staticmethod
    def exceptionToErrorStack(throwable: java.lang.Throwable) -> java.lang.Iterable[str]: ...
    def getErrorStack(self) -> java.util.List[str]: ...
    def getErrorType(self) -> str: ...
    def getMessage(self) -> str: ...
    def getTimestamp(self) -> java.time.OffsetDateTime: ...

class ServerResponseException(java.lang.RuntimeException):
    """
    public class ServerResponseException extends java.lang.RuntimeException
    
    
        Also see:
            :meth:`~serialized`
    """
    @typing.overload
    def __init__(self, int: int, errorDto: ErrorDto): ...
    @typing.overload
    def __init__(self, int: int, errorDto: ErrorDto, throwable: java.lang.Throwable): ...
    def getErrorStack(self) -> java.util.List[str]: ...
    def getErrorType(self) -> str: ...
    def getMessage(self) -> str:
        """
        
            Overrides:
                :code:`getMessage` in class :code:`java.lang.Throwable`
        
        
        """
        ...
    def getServerCode(self) -> int: ...
    def getTimestamp(self) -> java.time.OffsetDateTime: ...

class DefaultErrorDto(ErrorDto):
    @staticmethod
    def builder() -> 'DefaultErrorDto.Builder': ...
    @staticmethod
    def copyOf(errorDto: ErrorDto) -> 'DefaultErrorDto': ...
    def equals(self, object: typing.Any) -> bool: ...
    def getErrorStack(self) -> java.util.List[str]: ...
    def getErrorType(self) -> str: ...
    def getMessage(self) -> str: ...
    def getTimestamp(self) -> java.time.OffsetDateTime: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...
    @typing.overload
    def withErrorStack(self, iterable: java.lang.Iterable[str]) -> 'DefaultErrorDto': ...
    @typing.overload
    def withErrorStack(self, *string: str) -> 'DefaultErrorDto': ...
    def withErrorType(self, string: str) -> 'DefaultErrorDto': ...
    def withMessage(self, string: str) -> 'DefaultErrorDto': ...
    def withTimestamp(self, offsetDateTime: java.time.OffsetDateTime) -> 'DefaultErrorDto': ...
    class Builder:
        def addAllErrorStack(self, iterable: java.lang.Iterable[str]) -> 'DefaultErrorDto.Builder': ...
        @typing.overload
        def addErrorStack(self, string: str) -> 'DefaultErrorDto.Builder': ...
        @typing.overload
        def addErrorStack(self, *string: str) -> 'DefaultErrorDto.Builder': ...
        def build(self) -> 'DefaultErrorDto': ...
        def errorStack(self, iterable: java.lang.Iterable[str]) -> 'DefaultErrorDto.Builder': ...
        def errorType(self, string: str) -> 'DefaultErrorDto.Builder': ...
        def from_(self, errorDto: ErrorDto) -> 'DefaultErrorDto.Builder': ...
        def message(self, string: str) -> 'DefaultErrorDto.Builder': ...
        def timestamp(self, offsetDateTime: java.time.OffsetDateTime) -> 'DefaultErrorDto.Builder': ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.rest.core.errors")``.

    DefaultErrorDto: typing.Type[DefaultErrorDto]
    ErrorDto: typing.Type[ErrorDto]
    ServerResponseException: typing.Type[ServerResponseException]
