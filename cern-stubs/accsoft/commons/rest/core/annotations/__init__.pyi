
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import java.lang.annotation
import typing



class EnumAsJsonObject(java.lang.annotation.Annotation):
    """
    @Target(TYPE) @Retention(RUNTIME) public @interface EnumAsJsonObject
    """
    def equals(self, object: typing.Any) -> bool: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...

class HasUUIDInfo(java.lang.annotation.Annotation):
    """
    @Target(TYPE) @Retention(RUNTIME) public @interface HasUUIDInfo
    """
    def equals(self, object: typing.Any) -> bool: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.rest.core.annotations")``.

    EnumAsJsonObject: typing.Type[EnumAsJsonObject]
    HasUUIDInfo: typing.Type[HasUUIDInfo]
