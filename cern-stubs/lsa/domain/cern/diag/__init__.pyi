
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.accsoft.commons.diag
import cern.accsoft.commons.diag.matcher
import java.lang
import typing



class LsaDriveExceptionThrowableMatcher(cern.accsoft.commons.diag.matcher.ExceptionClassThrowableMatcher):
    def __init__(self): ...

class LsaDriveProxyThrowableMatcher(cern.accsoft.commons.diag.matcher.ProxyThrowableMatcher):
    def __init__(self): ...

class LsaDriveStringThrowableMatcher(cern.accsoft.commons.diag.matcher.StringThrowableMatcher):
    def __init__(self): ...

class LsaDriveThrowableMessageComposer(cern.accsoft.commons.diag.DefaultThrowableMessageComposer):
    def __init__(self): ...
    def composeMessage(self, throwable: java.lang.Throwable) -> str: ...

class LsaStringThrowableMatcher(cern.accsoft.commons.diag.matcher.StringThrowableMatcher):
    LSA_PROBLEM_DOMAIN: typing.ClassVar[str] = ...
    def __init__(self): ...
    def buildThrowableDescriptor(self, throwable: java.lang.Throwable, throwable2: java.lang.Throwable) -> cern.accsoft.commons.diag.ThrowableDescriptor: ...

class LsaThrowableResolver(cern.accsoft.commons.diag.DefaultThrowableResolver):
    @staticmethod
    def getInstance() -> 'LsaThrowableResolver': ...

class LsaTrimExceptionThrowableMatcher(cern.accsoft.commons.diag.matcher.ExceptionClassThrowableMatcher):
    def __init__(self): ...

class LsaTrimProxyThrowableMatcher(cern.accsoft.commons.diag.matcher.ProxyThrowableMatcher):
    def __init__(self): ...

class LsaTrimStringThrowableMatcher(cern.accsoft.commons.diag.matcher.StringThrowableMatcher):
    def __init__(self): ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.lsa.domain.cern.diag")``.

    LsaDriveExceptionThrowableMatcher: typing.Type[LsaDriveExceptionThrowableMatcher]
    LsaDriveProxyThrowableMatcher: typing.Type[LsaDriveProxyThrowableMatcher]
    LsaDriveStringThrowableMatcher: typing.Type[LsaDriveStringThrowableMatcher]
    LsaDriveThrowableMessageComposer: typing.Type[LsaDriveThrowableMessageComposer]
    LsaStringThrowableMatcher: typing.Type[LsaStringThrowableMatcher]
    LsaThrowableResolver: typing.Type[LsaThrowableResolver]
    LsaTrimExceptionThrowableMatcher: typing.Type[LsaTrimExceptionThrowableMatcher]
    LsaTrimProxyThrowableMatcher: typing.Type[LsaTrimProxyThrowableMatcher]
    LsaTrimStringThrowableMatcher: typing.Type[LsaTrimStringThrowableMatcher]
