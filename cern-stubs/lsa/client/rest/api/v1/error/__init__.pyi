
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import com.fasterxml.jackson.databind
import feign
import feign.codec
import java.lang
import typing



class DefaultErrorDecoder(feign.codec.ErrorDecoder.Default):
    def __init__(self, objectMapper: com.fasterxml.jackson.databind.ObjectMapper): ...
    def decode(self, string: str, response: feign.Response) -> java.lang.Exception: ...

class ResourceNotFoundException(java.lang.RuntimeException):
    def __init__(self, string: str): ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.lsa.client.rest.api.v1.error")``.

    DefaultErrorDecoder: typing.Type[DefaultErrorDecoder]
    ResourceNotFoundException: typing.Type[ResourceNotFoundException]
