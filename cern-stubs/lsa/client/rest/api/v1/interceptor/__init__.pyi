
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import feign
import typing



class RbacInterceptor(feign.RequestInterceptor):
    def __init__(self): ...
    def apply(self, requestTemplate: feign.RequestTemplate) -> None: ...

class TracingInterceptor(feign.RequestInterceptor):
    def __init__(self): ...
    def apply(self, requestTemplate: feign.RequestTemplate) -> None: ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.lsa.client.rest.api.v1.interceptor")``.

    RbacInterceptor: typing.Type[RbacInterceptor]
    TracingInterceptor: typing.Type[TracingInterceptor]
