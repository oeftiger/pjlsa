
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.lsa.client.rest.cern.api
import typing


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.lsa.client.rest.cern")``.

    api: cern.lsa.client.rest.cern.api.__module_protocol__
