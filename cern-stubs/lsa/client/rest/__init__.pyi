
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.lsa.client.rest.api
import cern.lsa.client.rest.cern
import typing


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.lsa.client.rest")``.

    api: cern.lsa.client.rest.api.__module_protocol__
    cern: cern.lsa.client.rest.cern.__module_protocol__
