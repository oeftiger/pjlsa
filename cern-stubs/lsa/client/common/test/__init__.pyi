
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import typing



class MockServiceLocator:
    """
    public final class MockServiceLocator extends java.lang.Object
    
        The class can be used to mock LSA services. It is used by the :code:`ServiceLocator` to check if there is a mock
        instance of a requested bean. The typical usage would be like this:
    
        .. code-block: java
        
         OpticService ctrl = mock(OpticService.class);
         when(ctrl.findAccelerators()).thenReturn(ACCELERATORS);
         //Set the mock implementation of the controller      
         MockServiceLocator.setMockBean(OpticService.class, ctrl);
    """
    _getMockBean_0__T = typing.TypeVar('_getMockBean_0__T')  # <T>
    @typing.overload
    @staticmethod
    def getMockBean(class_: typing.Type[_getMockBean_0__T]) -> _getMockBean_0__T:
        """
            Returns bean registered for the specified type (interface).
        
            Parameters:
                interfaceType (java.lang.Class<T> interfaceType): interface type
        
            Returns:
                bean registered for this interface
        
        """
        ...
    @typing.overload
    @staticmethod
    def getMockBean(string: str) -> typing.Any:
        """
            Returns bean registered for the specified interface name. The :code:`ServiceLocator` uses this method to check if there
            is a mock bean registered for a given interface and returns it if there is one.
        
            Parameters:
                interfaceName (java.lang.String): decapitalized :code:`simple name` of the interface class
        
            Returns:
                bean registered for this name
        
        
        """
        ...
    _setMockBean__T = typing.TypeVar('_setMockBean__T')  # <T>
    @staticmethod
    def setMockBean(class_: typing.Type[_setMockBean__T], t: _setMockBean__T) -> None:
        """
            Sets mock implementation of the specified type (interface) that is returned by the :code:`ServiceLocator`.
        
            Parameters:
                interfaceType (java.lang.Class<T> interfaceType): interface type
                bean (T): the bean to be returned
        
        
        """
        ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.lsa.client.common.test")``.

    MockServiceLocator: typing.Type[MockServiceLocator]
