
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.lsa.domain.commons.logging
import java.lang
import java.lang.reflect
import jpype
import org.springframework.beans.factory
import org.springframework.remoting.rmi
import org.springframework.remoting.support
import typing



class LSAServiceAwareRmiProxyFactoryBean(org.springframework.remoting.rmi.RmiProxyFactoryBean, org.springframework.beans.factory.BeanNameAware):
    def __init__(self, string: str, int: int): ...
    def afterPropertiesSet(self) -> None: ...
    def getObject(self) -> typing.Any: ...
    def setBeanName(self, string: str) -> None: ...
    def setInterfaceClassName(self, string: str) -> None: ...
    def setLogicalInterfaceName(self, string: str) -> None: ...
    class FilteringStackTraceInvocationHandler(java.lang.reflect.InvocationHandler):
        PROPERTY_STACKTRACE_FILTER_DISABLED: typing.ClassVar[str] = ...
        PROPERTY_STACKTRACE_FILTER_PATTERNS: typing.ClassVar[str] = ...
        def invoke(self, object: typing.Any, method: java.lang.reflect.Method, objectArray: typing.Union[typing.List[typing.Any], jpype.JArray]) -> typing.Any: ...

class LoggingAwareRemoteInvocationResult(org.springframework.remoting.support.RemoteInvocationResult):
    @typing.overload
    def __init__(self, object: typing.Any): ...
    @typing.overload
    def __init__(self, throwable: java.lang.Throwable): ...
    def getMiddleTierLogs(self) -> typing.MutableSequence[cern.lsa.domain.commons.logging.LogMessage]: ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.lsa.client.common.spi.remoting")``.

    LSAServiceAwareRmiProxyFactoryBean: typing.Type[LSAServiceAwareRmiProxyFactoryBean]
    LoggingAwareRemoteInvocationResult: typing.Type[LoggingAwareRemoteInvocationResult]
