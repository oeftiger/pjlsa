
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern
import cern.accsoft.commons.domain
import typing



_LsaServiceResolver__T = typing.TypeVar('_LsaServiceResolver__T')  # <T>
class LsaServiceResolver(typing.Generic[_LsaServiceResolver__T]):
    """
    public interface LsaServiceResolver<T>
    
        Resolves a particular LSA service for particular accelerator.
    """
    def getService(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> _LsaServiceResolver__T: ...

_TestLsaServiceResolver__T = typing.TypeVar('_TestLsaServiceResolver__T')  # <T>
class TestLsaServiceResolver(LsaServiceResolver[_TestLsaServiceResolver__T], typing.Generic[_TestLsaServiceResolver__T]):
    """
    public class TestLsaServiceResolver<T> extends java.lang.Object implements :class:`~cern.lsa.client.resolver.LsaServiceResolver`<T>
    """
    def __init__(self, class_: typing.Type[_TestLsaServiceResolver__T]): ...
    def getService(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> _TestLsaServiceResolver__T:
        """
        
            Specified by:
                :meth:`~cern.lsa.client.resolver.LsaServiceResolver.getService`Â in
                interfaceÂ :class:`~cern.lsa.client.resolver.LsaServiceResolver`
        
        
        """
        ...

_LocalLsaServiceResolver__T = typing.TypeVar('_LocalLsaServiceResolver__T')  # <T>
class LocalLsaServiceResolver(cern.lsa.client.resolver.AbstractLsaServiceResolver[_LocalLsaServiceResolver__T], typing.Generic[_LocalLsaServiceResolver__T]):
    """
    public class LocalLsaServiceResolver<T> extends java.lang.Object
    """
    def __init__(self, class_: typing.Type[_LocalLsaServiceResolver__T]): ...

_NextLsaServiceResolver__T = typing.TypeVar('_NextLsaServiceResolver__T')  # <T>
class NextLsaServiceResolver(cern.lsa.client.resolver.AbstractLsaServiceResolver[_NextLsaServiceResolver__T], typing.Generic[_NextLsaServiceResolver__T]):
    """
    public class NextLsaServiceResolver<T> extends java.lang.Object
    """
    def __init__(self, class_: typing.Type[_NextLsaServiceResolver__T]): ...

_ProLsaServiceResolver__T = typing.TypeVar('_ProLsaServiceResolver__T')  # <T>
class ProLsaServiceResolver(cern.lsa.client.resolver.AbstractLsaServiceResolver[_ProLsaServiceResolver__T], typing.Generic[_ProLsaServiceResolver__T]):
    """
    public class ProLsaServiceResolver<T> extends java.lang.Object
    """
    def __init__(self, class_: typing.Type[_ProLsaServiceResolver__T]): ...

_TestbedLsaServiceResolver__T = typing.TypeVar('_TestbedLsaServiceResolver__T')  # <T>
class TestbedLsaServiceResolver(cern.lsa.client.resolver.AbstractLsaServiceResolver[_TestbedLsaServiceResolver__T], typing.Generic[_TestbedLsaServiceResolver__T]):
    """
    public class TestbedLsaServiceResolver<T> extends java.lang.Object
    """
    def __init__(self, class_: typing.Type[_TestbedLsaServiceResolver__T]): ...

class AbstractLsaServiceResolver: ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.lsa.client.resolver")``.

    AbstractLsaServiceResolver: typing.Type[AbstractLsaServiceResolver]
    LocalLsaServiceResolver: typing.Type[LocalLsaServiceResolver]
    LsaServiceResolver: typing.Type[LsaServiceResolver]
    NextLsaServiceResolver: typing.Type[NextLsaServiceResolver]
    ProLsaServiceResolver: typing.Type[ProLsaServiceResolver]
    TestLsaServiceResolver: typing.Type[TestLsaServiceResolver]
    TestbedLsaServiceResolver: typing.Type[TestbedLsaServiceResolver]
