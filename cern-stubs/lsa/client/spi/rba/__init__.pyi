
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.rbac.common
import java.util.concurrent
import typing



class RbaUtils:
    """
    public class RbaUtils extends java.lang.Object
    
        Utility class containing common methods dealing with RBAC
    """
    def __init__(self): ...
    _fireActionWithRbaToken__T = typing.TypeVar('_fireActionWithRbaToken__T')  # <T>
    @staticmethod
    def fireActionWithRbaToken(callable: typing.Union[java.util.concurrent.Callable[_fireActionWithRbaToken__T], typing.Callable[[], _fireActionWithRbaToken__T]], rbaToken: cern.rbac.common.RbaToken) -> _fireActionWithRbaToken__T: ...
    _fireActionWithRbaTokenInSeparateThread__T = typing.TypeVar('_fireActionWithRbaTokenInSeparateThread__T')  # <T>
    @staticmethod
    def fireActionWithRbaTokenInSeparateThread(callable: typing.Union[java.util.concurrent.Callable[_fireActionWithRbaTokenInSeparateThread__T], typing.Callable[[], _fireActionWithRbaTokenInSeparateThread__T]], rbaToken: cern.rbac.common.RbaToken) -> java.util.concurrent.Future[_fireActionWithRbaTokenInSeparateThread__T]:
        """
            Runs the given :code:`Callable` with the specified :code:`RbaToken` in a separate thread (using :code:`ExecutorService`)
        
            Parameters:
                action (java.util.concurrent.Callable<T> action): task to be executed
                token (cern.rbac.common.RbaToken): used while the execution of the :code:`action`
        
            Returns:
                a :code:`Future` to the result of the :code:`action`
        
        
        """
        ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.lsa.client.spi.rba")``.

    RbaUtils: typing.Type[RbaUtils]
