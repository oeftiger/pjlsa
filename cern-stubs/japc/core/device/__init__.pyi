
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.accsoft.commons.util
import cern.japc.core
import cern.japc.core.device.impl
import cern.japc.core.transaction
import java.util
import typing



class JapcDevice(cern.accsoft.commons.util.Named):
    """
    public interface JapcDevice extends cern.accsoft.commons.util.Named
    
        JAPC device which is represented as a collection of parameters corresponding to its properties.
    """
    def getImmutableParameter(self, string: str) -> cern.japc.core.ImmutableParameter:
        """
        
            Parameters:
                propertyName (java.lang.String): 
            Returns:
                the parameter
        
            Raises:
                java.lang.IllegalArgumentException: if the parameter is not found in the device
        
        
        """
        ...
    def getParameter(self, string: str) -> cern.japc.core.Parameter:
        """
        
            Parameters:
                propertyName (java.lang.String): 
            Returns:
                the parameter
        
            Raises:
                java.lang.IllegalArgumentException: if the parameter is not found in the device or is not writable
        
        
        """
        ...
    def getPropertyNames(self) -> java.util.Set[str]:
        """
        
            Returns:
                property names
        
        
        """
        ...
    def getTransactionalParameter(self, string: str) -> cern.japc.core.transaction.TransactionalParameter:
        """
        
            Parameters:
                propertyName (java.lang.String): 
            Returns:
                the parameter
        
            Raises:
                java.lang.IllegalArgumentException: if the parameter is not found in the device or is not transactional
        
        
        """
        ...
    def getWritablePropertyNames(self) -> java.util.Set[str]:
        """
        
            Returns:
                writable property names
        
        
        """
        ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.japc.core.device")``.

    JapcDevice: typing.Type[JapcDevice]
    impl: cern.japc.core.device.impl.__module_protocol__
