
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import java.util
import typing



class BindInstance:
    """
    public abstract class BindInstance extends java.lang.Object
    """
    globalList: typing.ClassVar[java.util.HashMap] = ...
    """
    public static java.util.HashMap<java.lang.String, :class:`~cern.japc.core.directory.client.services.BindInstance`> globalList
    
    
    """
    def __init__(self): ...
    def checkExisting(self) -> bool: ...
    def getServerInfo(self) -> java.util.Properties: ...
    def toString(self) -> str:
        """
        
            Overrides:
                :code:`toString` in class :code:`java.lang.Object`
        
        
        """
        ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.japc.core.directory.client.services")``.

    BindInstance: typing.Type[BindInstance]
