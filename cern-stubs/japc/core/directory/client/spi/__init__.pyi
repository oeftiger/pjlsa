
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.cmw.directory.client
import java.util
import jpype
import typing



class ServerInfoUtil:
    """
    public class ServerInfoUtil extends java.lang.Object
    
        Utility methods to encode and decode the configuration of services into/from :code:`ServerInfo`.
    """
    MAX_SERVICE_URL_LENGTH: typing.ClassVar[int] = ...
    """
    public static final int MAX_SERVICE_URL_LENGTH
    
        The maximum allowed service url length
    
        Also see:
            :meth:`~constant`
    
    
    """
    def __init__(self): ...
    @staticmethod
    def getProperties(serverInfo: cern.cmw.directory.client.ServerInfo) -> typing.MutableSequence[java.util.Properties]: ...
    @staticmethod
    def getServerInfo(propertiesArray: typing.Union[typing.List[java.util.Properties], jpype.JArray]) -> cern.cmw.directory.client.ServerInfo: ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.japc.core.directory.client.spi")``.

    ServerInfoUtil: typing.Type[ServerInfoUtil]
