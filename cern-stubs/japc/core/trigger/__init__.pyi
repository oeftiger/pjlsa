
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.accsoft.commons.util.trigger
import cern.accsoft.commons.util.trigger.impl
import cern.japc.core.factory
import typing



_AbstractJapcTrigger__E = typing.TypeVar('_AbstractJapcTrigger__E', bound=cern.accsoft.commons.util.trigger.TriggerEvent)  # <E>
class AbstractJapcTrigger(cern.accsoft.commons.util.trigger.impl.AbstractTrigger[_AbstractJapcTrigger__E], typing.Generic[_AbstractJapcTrigger__E]):
    """
    public abstract class AbstractJapcTrigger<E extends cern.accsoft.commons.util.trigger.TriggerEvent> extends cern.accsoft.commons.util.trigger.impl.AbstractTrigger<E>
    
        Trigger implementation based on JAPC updates.
    """
    def __init__(self, string: str): ...
    def init(self) -> None: ...
    def setParameterFactory(self, parameterFactory: cern.japc.core.factory.ParameterFactory) -> None:
        """
        
            Parameters:
                parameterFactory (:class:`~cern.japc.core.factory.ParameterFactory`): :class:`~cern.japc.core.factory.ParameterFactory` to use
        
        
        """
        ...
    def setTriggerSelector(self, string: str) -> None:
        """
        
            Parameters:
                triggerSelector (java.lang.String): JAPC selector for triggering parameter subscription
        
        
        """
        ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.japc.core.trigger")``.

    AbstractJapcTrigger: typing.Type[AbstractJapcTrigger]
