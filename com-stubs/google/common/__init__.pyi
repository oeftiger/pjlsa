
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import com.google.common.base
import com.google.common.collect
import typing


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("com.google.common")``.

    base: com.google.common.base.__module_protocol__
    collect: com.google.common.collect.__module_protocol__
