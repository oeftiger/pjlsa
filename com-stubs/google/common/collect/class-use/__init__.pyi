import typing


class __module_protocol__(typing.Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("com.google.common.collect.class-use")``.

    pass
